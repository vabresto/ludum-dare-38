#ifndef WAVEMANGER_H
#define WAVEMANGER_H

#include <SFML/Graphics.hpp>
#include <vector>

#include "EnemyScout.h"
#include "EnemyArmoured.h"
#include "EnemySupport.h"
#include "EnemyCarrier.h"
#include "EnemyDropship.h"

struct Alert
{
	sf::Sprite msprite;
	sf::Text distanceText;
};

class WaveManger
{
	//std::vector<Enemy> waveEnemies;
	unsigned int numOfEnemies;

	//Probably easier to have a vector of each different enemy
	std::vector<EnemyScout> scoutsList;
	std::vector<EnemyArmoured> armouredList;
	std::vector<EnemySupport> supportList;
	std::vector<EnemyCarrier> carriersList;
	std::vector<EnemyDropship> dropshipsList;


	std::vector<Alert> alertsList;

	sf::Time timeUntilNextWaveStarts;
	sf::Text timeUntilNextWaveText;
	char nextWaveString[50];

public:
	WaveManger();
	~WaveManger();

	bool justCompletedWave;

	void SpawnNextWave();
	unsigned int GetNumEnemies();

	sf::Vector2f GetValidEnemySpawn();
	sf::Vector2f GetValidLargeEnemySpawn(bool _isCarrier);
	sf::Vector2f GetClosestEnemyLocation(sf::Vector2f _start);

	void Init();
	void Update(sf::Time _deltaTime);
	void Draw(sf::RenderWindow& _window);
	void DrawAlerts(sf::RenderWindow& _window);

	bool isInShieldBubble(sf::Vector2f _point);

	void CarrierSpawnDropship(sf::Vector2f _pos);

	bool didCarriersSpawn();
	sf::Vector2f GetClosestCarrier(sf::Vector2f _start);

	int GetNumEnemiesInRange(sf::Vector2f _center, float _radius, int _minClass);
	
	void Reset();
};

#endif // WAVEMANGER_H
