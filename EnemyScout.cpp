#include "EnemyScout.h"
#include "Globals.h"

EnemyScout::EnemyScout()
{
}

EnemyScout::~EnemyScout()
{
}

void EnemyScout::Init(sf::Vector2f _spawn)
{
	msprite.setTexture(scoutEnemyTexture);
	msprite.setOrigin(16.f,16.f);
	
	msprite.setPosition(_spawn);
	
	moveTarget = homeplanet.GetPosition();
	moveSpeed = 8.f;
	health = 1;
	
	length = 4;
	
	moneyValue = 25;
}

void EnemyScout::Update(sf::Time _deltaTime)
{
	timeSinceHitByExplosion += _deltaTime;
	isDamaged = false;

	if(damagedTimer.asSeconds() > 0)
	{
		damagedTimer -= _deltaTime;
	}
	

	sf::Vector2f dir = moveTarget;
	dir.x -= msprite.getPosition().x;
	dir.y -= msprite.getPosition().y;

	dir = Normalize(dir);
	dir.x *= moveSpeed;
	dir.y *= moveSpeed;

	msprite.setRotation(atan2(dir.y, dir.x) * 180 / 3.14159 + 90);
	msprite.setPosition(msprite.getPosition().x + dir.x, msprite.getPosition().y + dir.y);

	if(rand() % 7 < 5/* && exhPartsList.size() < 100*/)
	{
		ParticleExhaust exh;
		exh.Init(sf::Vector2f(msprite.getPosition().x - dir.x * length,
		                      msprite.getPosition().y - dir.y * length),
		         msprite.getRotation(), 0.75, 1);
		exhPartsList.push_back(exh);
	}

	if(!wavemanger.isInShieldBubble(msprite.getPosition()))
	{
		for(unsigned int cnt = 0; cnt < bulletsList.size(); cnt++)
		{
			if(msprite.getGlobalBounds().contains(bulletsList[cnt].msprite.getPosition()))
			{
				bulletsList[cnt].HitSomething();
				health--;
				soundmanager.CreateSound(SoundManager::ENEMYHIT);
				isDamaged = true;

				if(health <= 0)
				{
					moneyui.money += moneyValue;
				}
			}
		}
		for(unsigned int cnt = 0; cnt < explosionsList.size(); cnt++)
		{
			if(timeSinceHitByExplosion.asSeconds() > 0.5 && msprite.getGlobalBounds().intersects(explosionsList[cnt].GetGlobalBounds()))
			{
				timeSinceHitByExplosion = sf::Time::Zero;
				health --;
				soundmanager.CreateSound(SoundManager::ENEMYHIT);
				isDamaged = true;
				if(health <= 0)
				{
					moneyui.money += moneyValue;
				}
			}
		}
	}

	if(isDamaged)
	{
		damagedTimer = sf::seconds(0.5);
		msprite.setColor(sf::Color::Cyan);
	}
	else if(damagedTimer.asSeconds() <= 0)
	{
		msprite.setColor(sf::Color::White);
	}
}
