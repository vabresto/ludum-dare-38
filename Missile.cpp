#include "Missile.h"
#include <SFML/Graphics.hpp>
#include "Globals.h"
#include "Explosion.h"

Missile::Missile()
{
}

Missile::~Missile()
{
}

void Missile::Init(sf::Vector2f _spawn, float _ms, float _radius, float _theta)
{
	//HARDCODED
	moveSpeed = 11;
	radius = _radius + 16;
	theta = _theta;

	hasTarget = false;
	timeToLive = sf::seconds(8);

	prevDist = 100000;

	msprite.setTexture(missilesTexture);
	msprite.setPosition(_spawn);
	msprite.setOrigin(8.f, 14.f);
	msprite.setRotation(atan2(msprite.getPosition().y - homeplanet.GetPosition().y,
	                          msprite.getPosition().x - homeplanet.GetPosition().x) * 180 / 3.14159 + 90);
}

void Missile::Fire()
{
	hasTarget = true;
}

void Missile::Update(sf::Time _deltaTime)
{
	if(!hasTarget)
	{
		msprite.setPosition(homeplanet.GetPosition().x + radius * cos(homeplanet.GetRotation() / 180 * 3.14159 + theta),
		                    homeplanet.GetPosition().y + radius * sin(homeplanet.GetRotation() / 180 * 3.14159 + theta));

		msprite.setRotation(atan2(msprite.getPosition().y - homeplanet.GetPosition().y,
		                          msprite.getPosition().x - homeplanet.GetPosition().x) * 180 / 3.14159 + 90);
	}
	else
	{
		timeToLive -= _deltaTime;

		sf::Vector2f dir = wavemanger.GetClosestEnemyLocation(msprite.getPosition());

		if(prevDist < DistanceBetween(dir, msprite.getPosition()) || DistanceBetween(dir, msprite.getPosition()) < 16
		        || wavemanger.isInShieldBubble(msprite.getPosition()))
		{
			Explosion explosion;
			explosionsList.push_back(explosion);
			explosionsList[explosionsList.size() - 1].Init(msprite.getPosition());
			hasTarget = false;
		}

		prevDist = DistanceBetween(dir, msprite.getPosition());

		dir.x -= msprite.getPosition().x;
		dir.y -= msprite.getPosition().y;


		dir = Normalize(dir);
		dir.x *= moveSpeed;
		dir.y *= moveSpeed;

		msprite.setRotation(atan2(dir.y, dir.x) * 180 / 3.14159 + 90);
		msprite.setPosition(msprite.getPosition().x + dir.x, msprite.getPosition().y + dir.y);

		if(rand() % 6 < 5/* && exhPartsList.size() < 100*/)
		{
			ParticleExhaust exh;
			exh.Init(sf::Vector2f(msprite.getPosition().x - dir.x * 1.15 / moveSpeed,
			                      msprite.getPosition().y - dir.y * 1.15 / moveSpeed),
			         msprite.getRotation(), 0.5, 0);
			exhPartsList.push_back(exh);
		}

	}

}

bool Missile::IsStillAlive()
{
	return hasTarget && timeToLive.asSeconds() > 0 && wavemanger.GetNumEnemies() > 0;
}

void Missile::Draw(sf::RenderWindow& _window)
{
	_window.draw(msprite);
}
