#ifndef ENEMYCARRIER_H
#define ENEMYCARRIER_H

#include <SFML/Graphics.hpp>

#include "Enemy.h"

class EnemyCarrier : public Enemy
{
	int stage;
	
	int storedDropships;
	sf::Time timeSinceLastDrop;
	
public:
	EnemyCarrier();
	~EnemyCarrier();

	void Init(sf::Vector2f _spawn);
	void Update(sf::Time _deltaTime);
	
	float GetValue();
};

#endif // ENEMYCARRIER_H
