#include "EnemyCarrier.h"
#include "Globals.h"

EnemyCarrier::EnemyCarrier()
{
}

EnemyCarrier::~EnemyCarrier()
{
}

void EnemyCarrier::Init(sf::Vector2f _spawn)
{
	msprite.setTexture(carrierEnemyTexture);
	msprite.setOrigin(64.f, 80.f);

	msprite.setPosition(_spawn);

	moveTarget = homeplanet.GetPosition();
	moveSpeed = 3.f;
	health = 7;

	length = 26;

	moneyValue = 400;
	
	storedDropships = 7;	// + 1

	stage = 0;
}

void EnemyCarrier::Update(sf::Time _deltaTime)
{
	timeSinceLastDrop += _deltaTime;
	timeSinceHitByExplosion += _deltaTime;
	isDamaged = false;

	if(damagedTimer.asSeconds() > 0)
	{
		damagedTimer -= _deltaTime;
	}

	sf::Vector2f dir = moveTarget;
	dir.x -= msprite.getPosition().x;
	dir.y -= msprite.getPosition().y;

	dir = Normalize(dir);
	dir.x *= moveSpeed;
	dir.y *= moveSpeed;

	msprite.setRotation(atan2(dir.y, dir.x) * 180 / 3.14159 + 90);
	msprite.setPosition(msprite.getPosition().x + dir.x, msprite.getPosition().y + dir.y);

	if(rand() % 8 < 5/* && exhPartsList.size() < 100*/)
	{
		ParticleExhaust exh;
		exh.Init(sf::Vector2f(msprite.getPosition().x - dir.x * length,
		                      msprite.getPosition().y - dir.y * length),
		         msprite.getRotation(), 2, 1);
		exhPartsList.push_back(exh);
	}

	if(stage == 0 && DistanceBetween(msprite.getPosition(), homeplanet.GetPosition()) < 900)
	{
		moveSpeed = 0.00000001;
		stage = 1;
	}
	if (stage == 1 && timeSinceLastDrop > sf::seconds(0.5))
	{
		timeSinceLastDrop = sf::Time::Zero;
		storedDropships --;
		wavemanger.CarrierSpawnDropship(msprite.getPosition());
		
		if (storedDropships < 0)
		{
			stage = 2;
			moveSpeed = 3.5f;
		}
	}


	if(!wavemanger.isInShieldBubble(msprite.getPosition()))
	{
		for(unsigned int cnt = 0; cnt < bulletsList.size(); cnt++)
		{
			if(msprite.getGlobalBounds().contains(bulletsList[cnt].msprite.getPosition()))
			{
				bulletsList[cnt].HitSomething();
				health--;
				soundmanager.CreateSound(SoundManager::ENEMYHIT);
				isDamaged = true;

				if(health <= 0)
				{
					moneyui.money += moneyValue + storedDropships * 25;
				}
			}
		}
		for(unsigned int cnt = 0; cnt < explosionsList.size(); cnt++)
		{
			if(timeSinceHitByExplosion.asSeconds() > 0.5 && msprite.getGlobalBounds().intersects(explosionsList[cnt].GetGlobalBounds()))
			{
				timeSinceHitByExplosion = sf::Time::Zero;
				health --;
				soundmanager.CreateSound(SoundManager::ENEMYHIT);
				isDamaged = true;
				if(health <= 0)
				{
					moneyui.money += moneyValue + storedDropships * 25;
				}
			}
		}
	}

	if(isDamaged)
	{
		damagedTimer = sf::seconds(0.5);
		msprite.setColor(sf::Color::Cyan);
	}
	else if(damagedTimer.asSeconds() <= 0)
	{
		msprite.setColor(sf::Color::White);
	}
}

float EnemyCarrier::GetValue()
{
	return moneyValue + storedDropships * 25;
}
