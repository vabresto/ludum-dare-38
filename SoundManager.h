#ifndef SOUNDMANAGER_H
#define SOUNDMANAGER_H

#include <SFML/Audio.hpp>
#include <vector>

class SoundManager
{
public:
	SoundManager();
	~SoundManager();
	
	enum SoundEffect{
		GAMEOVER, LEVELCOMPLETE, STARTBUILDING, DONEBUILDING, 
		PLANETHIT, PLANETRECOVERED, ALLIEDSHIELDREGEN, ENEMYSHIELDREGEN, 
		MISSILEEXPLODE, SHOOT, POWERDOWN, ENEMYDIED, ENEMYSHIELDHIT, ENEMYHIT, MOUSEOVER
	};
	

	std::vector<sf::Sound> soundsList;
	
	void CreateSound(SoundEffect _request);
	void Update();
};

#endif // SOUNDMANAGER_H
