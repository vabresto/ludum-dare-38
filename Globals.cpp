#include <SFML/Audio.hpp>
#include <math.h>
#include <iostream>
#include "Globals.h"
#include "Player.h"
#include "BuyBar.h"

unsigned int highscore, score;
sf::RenderWindow window(sf::VideoMode(800, 600), "HOMEBASE: DEFENCE");
sf::View mainView(sf::FloatRect(400, 300, 800, 600));

MiscUI miscui;

std::vector<GameObject*> objectsList;
std::vector<ParticleExhaust> exhPartsList;
std::vector<Bullet> bulletsList;
std::vector<Missile> missilesList;
std::vector<Explosion> explosionsList;

sf::Texture planetsTexture;
sf::Texture itemsTexture;
sf::Texture exhaustGoodTexture;
sf::Texture exhaustEnemyTexture;
sf::Texture buildingsTexture;
sf::Texture bulletsTexture;
sf::Texture missilesTexture;
sf::Texture progressBarCoverTexture;
sf::Texture progressBarTexture;
sf::Texture alertTexture;
sf::Texture explosionTexture;
sf::Texture pauseOverlayTexture;
sf::Texture energyBarCoverTexture;
sf::Texture energyBarTexture;
sf::Texture shieldBubbleTexture;

sf::Texture scoutEnemyTexture;
sf::Texture armouredEnemyTexture;
sf::Texture supportEnemyTexture;
sf::Texture supportEnemyShieldTexture;
sf::Texture carrierEnemyTexture;
sf::Texture dropshipEnemyTexture;

sf::Font mainFont;
sf::Font subFont;

WaveManger wavemanger;
Player player;
Planet homeplanet;
BuyBar buybar;
MoneyUI moneyui;
SoundManager soundmanager;

sf::Clock gameClock;
const sf::Time timePerFrame = sf::seconds(1.f / 30.f);
sf::Time timeAcc;

sf::Vector2f MousePos;



sf::SoundBuffer gameoverBuffer;
sf::SoundBuffer levelCompleteBuffer;
sf::SoundBuffer levelComplete2Buffer;
sf::SoundBuffer startBuildingBuffer;
sf::SoundBuffer doneBuildingBuffer;
sf::SoundBuffer planetHitBuffer;
sf::SoundBuffer planetRecoveredBuffer;
sf::SoundBuffer alliedShieldRegenBuffer;
sf::SoundBuffer enemyShieldRegenBuffer;
sf::SoundBuffer missileExplodeBuffer;
sf::SoundBuffer shoot1Buffer;
sf::SoundBuffer shoot2Buffer;
sf::SoundBuffer powerdownBuffer;
sf::SoundBuffer enemyDiedBuffer;
sf::SoundBuffer mouseoverBuffer;
sf::SoundBuffer hitEnemyBuffer;
sf::SoundBuffer hitEnemyShieldBuffer;



float DistanceBetween(sf::Vector2f _a, sf::Vector2f _b)
{
	return sqrt(pow(_a.x - _b.x, 2) + pow(_a.y - _b.y, 2));
}
sf::Vector2f Normalize(sf::Vector2f _a)
{
	float length = sqrt(pow(_a.x, 2) + pow(_a.y, 2));
	return sf::Vector2f(_a.x / length, _a.y / length);
}
sf::Vector2f RotationToDirection(float _rot)
{
	_rot -= 90;

	//std::cout << "Rotation " << _rot << std::endl;
	//std::cout << "Direction " << cos(_rot / 180 * 3.14159) * 180 / 3.14159 << " " << sin(_rot / 180 * 3.14159) * 180 / 3.14159 << std::endl;

	return sf::Vector2f(cos(_rot / 180 * 3.14159) * 180 / 3.14159, sin(_rot / 180 * 3.14159) * 180 / 3.14159);
}
