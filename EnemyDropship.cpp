#include "EnemyDropship.h"
#include "Globals.h"

EnemyDropship::EnemyDropship()
{
}

EnemyDropship::~EnemyDropship()
{
}

void EnemyDropship::Init(sf::Vector2f _spawn)
{
	msprite.setTexture(dropshipEnemyTexture);
	msprite.setOrigin(10.f,16.f);
	
	msprite.setPosition(_spawn);
	
	moveTarget = homeplanet.GetPosition();
	moveSpeed = 5.f;
	health = 1;
	
	length = 3;
	
	moneyValue = 15;
}
