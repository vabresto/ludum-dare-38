#include "Icon.h"
#include "Globals.h"

Icon::Icon()
{
}

Icon::~Icon()
{
}

void Icon::Init(sf::Texture& _texture, sf::IntRect _rect, sf::Vector2f _offset, Building::StructureType _structuretype, float _cost)
{
	textureBounds = _rect;
	msprite.setTexture(_texture);
	msprite.setTextureRect(textureBounds);
	
	msprite.setScale(2.f,2.f);
	
	isMouseOver = false;
	
	cost = _cost;
	offset = _offset;
	structuretype = _structuretype;
}

void Icon::Update(sf::Time _deltaTime)
{
	if (msprite.getGlobalBounds().contains(MousePos) && !isMouseOver && !homeplanet.isBuilding && moneyui.money >= cost)
	{
		soundmanager.CreateSound(SoundManager::MOUSEOVER);
		isMouseOver = true;
		textureBounds.left += 32;
		msprite.setTextureRect(textureBounds);
	}
	else if (isMouseOver && (!msprite.getGlobalBounds().contains(MousePos) || homeplanet.isBuilding))
	{
		isMouseOver = false;
		textureBounds.left -= 32;
		msprite.setTextureRect(textureBounds);
	}
	
	if (isMouseOver && sf::Mouse::isButtonPressed(sf::Mouse::Left))
	{
		homeplanet.ConstructBuilding(structuretype);
	}
	
	msprite.setPosition(buybar.GetPosition().x + offset.x, buybar.GetPosition().y + offset.y);
}

void Icon::Draw(sf::RenderWindow& _window)
{
	_window.draw(msprite);
}

bool Icon::CanBuy()
{
	return moneyui.money >= cost;
}
