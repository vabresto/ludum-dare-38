#ifndef ENEMYDROPSHIP_H
#define ENEMYDROPSHIP_H

#include "Enemy.h"

class EnemyDropship : public Enemy
{
public:
	EnemyDropship();
	~EnemyDropship();

	void Init(sf::Vector2f _spawn);
};

#endif // ENEMYDROPSHIP_H
