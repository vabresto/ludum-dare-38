#include <iostream>
#include <vector>
#include <random>
#include <algorithm>
#include <stdio.h>
#include <math.h>


#include "Globals.h"
#include "WaveManger.h"
#include "EnemyScout.h"
#include "EnemyArmoured.h"
#include "EnemySupport.h"
#include "EnemyCarrier.h"
#include "EnemyDropship.h"


int WaveTargetCost(int x)
{
	int ret = (int)((0.5 * (0.37 * x * sin(1.5 * x) + 0.3 * x) * (cos(0.65 * x)) + 0.65 * x) * 50);
	return  ret > 100 ? ret - 85 : ret;
}

WaveManger::WaveManger()
{
	for(unsigned int cnt = 1; cnt < 20; cnt++)
	{
		int waveDifficulty = WaveTargetCost(cnt);
		int numScouts = 0;
		int numArmoured = 0;
		int numSupport = 0;
		int numCarrier = 0;

		while(waveDifficulty > 500)
		{
			waveDifficulty /= 2;
			numCarrier++;
			numSupport++;
			numSupport++;
		}
		while(waveDifficulty > 75)
		{
			waveDifficulty /= 1.25;
			numArmoured++;
		}
		while(waveDifficulty >= 10)
		{
			waveDifficulty -= 10;
			numScouts++;
		}

		/*
		std::cout << "Level " << cnt << ": " << WaveTargetCost(cnt) << std::endl;
		std::cout << "Scouts:\t " << numScouts << std::endl;
		std::cout << "Armour:\t " << numArmoured << std::endl;
		std::cout << "Suppor:\t " << numSupport << std::endl;
		std::cout << "Carrie:\t " << numCarrier << std::endl << std::endl;
		*/
	}
}

WaveManger::~WaveManger()
{
}

void WaveManger::SpawnNextWave()
{
	int waveDifficulty = WaveTargetCost(moneyui.waveNumber);
	int numScouts = 0;
	int numArmoured = 0;
	int numSupport = 0;
	int numCarrier = 0;

	while(waveDifficulty > 500)
	{
		waveDifficulty /= 2;
		numCarrier++;
		numSupport++;
		numSupport++;
	}
	while(waveDifficulty > 75)
	{
		waveDifficulty /= 1.25;
		numArmoured++;
	}
	while(waveDifficulty >= 10)
	{
		waveDifficulty -= 10;
		numScouts++;
	}

	for(int cnt = 0; cnt < numCarrier; cnt++)
	{
		EnemyCarrier drop;
		drop.Init(GetValidLargeEnemySpawn(true));
		carriersList.push_back(drop);
	}
	for(int cnt = 0; cnt < numSupport; cnt++)
	{
		EnemySupport support;
		support.Init(GetValidLargeEnemySpawn(false));
		supportList.push_back(support);
	}
	for(int cnt = 0; cnt < numArmoured; cnt++)
	{
		EnemyArmoured armoured;
		armoured.Init(GetValidEnemySpawn());
		armouredList.push_back(armoured);
	}
	for(int cnt = 0; cnt < numScouts; cnt++)
	{
		EnemyScout scout;
		scout.Init(GetValidEnemySpawn());
		scoutsList.push_back(scout);
	}
}

void WaveManger::Init()
{
	timeUntilNextWaveStarts = sf::seconds(5);

	timeUntilNextWaveText.setFont(mainFont);
	timeUntilNextWaveText.setCharacterSize(35);
	timeUntilNextWaveText.setString("NEXT WAVE IN 00 SECONDS");
	timeUntilNextWaveText.setOrigin(timeUntilNextWaveText.getGlobalBounds().width / 2, 0.f);

	justCompletedWave = false;
}

void WaveManger::Update(sf::Time _deltaTime)
{
	alertsList.clear();
	numOfEnemies = 0;
	sf::FloatRect viewRegion(mainView.getCenter().x - mainView.getSize().x / 2,
	                         mainView.getCenter().y - mainView.getSize().y / 2,
	                         mainView.getSize().x, mainView.getSize().y);

	float radius = std::min(mainView.getSize().x / 2, mainView.getSize().y / 2);

	// TODO: Loop through each vector and update everything
	// INCLUDING DELETING DEAD ENEMIES!
	for(unsigned int cnt = 0; cnt < scoutsList.size(); cnt++)
	{
		numOfEnemies++;
		scoutsList[cnt].Update(_deltaTime);

		if(!wavemanger.isInShieldBubble(scoutsList[cnt].msprite.getPosition()))
		{
			if(scoutsList[cnt].msprite.getGlobalBounds().intersects(player.GetGlobalBounds()))
			{
				player.ResetPlayer();
				scoutsList[cnt].health = -1;

				moneyui.money = std::max(0, (int)(moneyui.money - 2 * scoutsList[cnt].GetValue()));
			}
		}

		if(!viewRegion.contains(scoutsList[cnt].msprite.getPosition()))
		{
			sf::Sprite newAlert;
			newAlert.setTexture(alertTexture);

			sf::Vector2f dir(scoutsList[cnt].msprite.getPosition().x - player.GetPosition().x,
			                 scoutsList[cnt].msprite.getPosition().y - player.GetPosition().y);
			dir = Normalize(dir);

			newAlert.setRotation(atan2(dir.y, dir.x) * 180 / 3.14159 + 90);
			newAlert.setPosition(mainView.getCenter().x + radius * dir.x,
			                     mainView.getCenter().y + radius * dir.y);

			sf::Text dist;
			dist.setFont(subFont);
			dist.setColor(sf::Color::Red);
			dist.setCharacterSize(20);
			dist.setString(std::to_string((int)DistanceBetween(scoutsList[cnt].msprite.getPosition(), player.GetPosition())));
			dist.setPosition(newAlert.getPosition().x - 30 * dir.x, newAlert.getPosition().y - 30 * dir.y);

			Alert alert;
			alert.msprite = newAlert;
			alert.distanceText = dist;

			alertsList.push_back(alert);
		}

		if(!scoutsList[cnt].IsStillAlive())
		{
			soundmanager.CreateSound(SoundManager::ENEMYDIED);
			scoutsList.erase(scoutsList.begin() + cnt);
			--cnt;
		}
		else if(homeplanet.isInFriendlyShieldBubble(scoutsList[cnt].GetPosition()))
		{
			homeplanet.ShieldHit(scoutsList[cnt].GetPosition(), scoutsList[cnt].GetValue());
			scoutsList[cnt].health = -1;
		}
		else if(homeplanet.GetHitbox().getGlobalBounds().contains(scoutsList[cnt].GetPosition()))
		{
			homeplanet.TakeDamage(1);
			scoutsList[cnt].health = -1;
		}
	}


	// ARMOURED ENEMIES
	for(unsigned int cnt = 0; cnt < armouredList.size(); cnt++)
	{
		numOfEnemies++;
		armouredList[cnt].Update(_deltaTime);

		if(!wavemanger.isInShieldBubble(armouredList[cnt].msprite.getPosition()))
		{
			if(armouredList[cnt].msprite.getGlobalBounds().intersects(player.GetGlobalBounds()))
			{
				player.ResetPlayer();
				armouredList[cnt].health = -1;

				moneyui.money = std::max(0, (int)(moneyui.money - 2 * armouredList[cnt].GetValue()));
			}
		}

		if(!viewRegion.contains(armouredList[cnt].msprite.getPosition()))
		{
			sf::Sprite newAlert;
			newAlert.setTexture(alertTexture);

			sf::Vector2f dir(armouredList[cnt].msprite.getPosition().x - player.GetPosition().x,
			                 armouredList[cnt].msprite.getPosition().y - player.GetPosition().y);
			dir = Normalize(dir);

			newAlert.setRotation(atan2(dir.y, dir.x) * 180 / 3.14159 + 90);
			newAlert.setPosition(mainView.getCenter().x + radius * dir.x,
			                     mainView.getCenter().y + radius * dir.y);

			sf::Text dist;
			dist.setFont(subFont);
			dist.setColor(sf::Color::Red);
			dist.setCharacterSize(20);
			dist.setString(std::to_string((int)DistanceBetween(armouredList[cnt].msprite.getPosition(), player.GetPosition())));
			dist.setPosition(newAlert.getPosition().x - 30 * dir.x, newAlert.getPosition().y - 30 * dir.y);

			Alert alert;
			alert.msprite = newAlert;
			alert.distanceText = dist;

			alertsList.push_back(alert);
		}


		if(!armouredList[cnt].IsStillAlive())
		{
			soundmanager.CreateSound(SoundManager::ENEMYDIED);
			armouredList.erase(armouredList.begin() + cnt);
			--cnt;
		}
		else if(homeplanet.isInFriendlyShieldBubble(armouredList[cnt].GetPosition()))
		{
			homeplanet.ShieldHit(armouredList[cnt].GetPosition(), armouredList[cnt].GetValue());
			armouredList[cnt].health = -1;
		}
		else if(homeplanet.GetHitbox().getGlobalBounds().contains(armouredList[cnt].GetPosition()))
		{
			homeplanet.TakeDamage(1);
			armouredList[cnt].health = -1;
		}
	}

	// SUPPORT ENEMIES
	for(unsigned int cnt = 0; cnt < supportList.size(); cnt++)
	{
		numOfEnemies++;
		supportList[cnt].Update(_deltaTime);

		if(!wavemanger.isInShieldBubble(supportList[cnt].msprite.getPosition()))
		{
			if(supportList[cnt].msprite.getGlobalBounds().intersects(player.GetGlobalBounds()))
			{
				player.ResetPlayer();
				supportList[cnt].health = -1;

				moneyui.money = std::max(0, (int)(moneyui.money - 2 * supportList[cnt].GetValue()));
			}
		}

		if(!viewRegion.contains(supportList[cnt].msprite.getPosition()))
		{
			sf::Sprite newAlert;
			newAlert.setTexture(alertTexture);

			sf::Vector2f dir(supportList[cnt].msprite.getPosition().x - player.GetPosition().x,
			                 supportList[cnt].msprite.getPosition().y - player.GetPosition().y);
			dir = Normalize(dir);

			newAlert.setRotation(atan2(dir.y, dir.x) * 180 / 3.14159 + 90);
			newAlert.setPosition(mainView.getCenter().x + radius * dir.x,
			                     mainView.getCenter().y + radius * dir.y);

			sf::Text dist;
			dist.setFont(subFont);
			dist.setColor(sf::Color::Red);
			dist.setCharacterSize(20);
			dist.setString(std::to_string((int)DistanceBetween(supportList[cnt].msprite.getPosition(), player.GetPosition())));
			dist.setPosition(newAlert.getPosition().x - 30 * dir.x, newAlert.getPosition().y - 30 * dir.y);

			Alert alert;
			alert.msprite = newAlert;
			alert.distanceText = dist;

			alertsList.push_back(alert);
		}


		if(!supportList[cnt].IsStillAlive())
		{
			soundmanager.CreateSound(SoundManager::ENEMYDIED);
			supportList.erase(supportList.begin() + cnt);
			--cnt;
		}
		else if(homeplanet.isInFriendlyShieldBubble(supportList[cnt].GetPosition()))
		{
			homeplanet.ShieldHit(supportList[cnt].GetPosition(), supportList[cnt].GetValue());
			supportList[cnt].health = -1;
		}
		else if(homeplanet.GetHitbox().getGlobalBounds().contains(supportList[cnt].GetPosition()))
		{
			homeplanet.TakeDamage(1);
			supportList[cnt].health = -1;
		}
	}

	// CARRIER ENEMIES
	for(unsigned int cnt = 0; cnt < carriersList.size(); cnt++)
	{
		numOfEnemies++;
		carriersList[cnt].Update(_deltaTime);

		if(!wavemanger.isInShieldBubble(carriersList[cnt].msprite.getPosition()))
		{
			if(carriersList[cnt].msprite.getGlobalBounds().intersects(player.GetGlobalBounds()))
			{
				player.ResetPlayer();
				carriersList[cnt].health = -1;

				moneyui.money = std::max(0, (int)(moneyui.money - 2 * carriersList[cnt].GetValue()));
			}
		}

		if(!viewRegion.contains(carriersList[cnt].msprite.getPosition()))
		{
			sf::Sprite newAlert;
			newAlert.setTexture(alertTexture);

			sf::Vector2f dir(carriersList[cnt].msprite.getPosition().x - player.GetPosition().x,
			                 carriersList[cnt].msprite.getPosition().y - player.GetPosition().y);
			dir = Normalize(dir);

			newAlert.setRotation(atan2(dir.y, dir.x) * 180 / 3.14159 + 90);
			newAlert.setPosition(mainView.getCenter().x + radius * dir.x,
			                     mainView.getCenter().y + radius * dir.y);

			sf::Text dist;
			dist.setFont(subFont);
			dist.setColor(sf::Color::Red);
			dist.setCharacterSize(20);
			dist.setString(std::to_string((int)DistanceBetween(carriersList[cnt].msprite.getPosition(), player.GetPosition())));
			dist.setPosition(newAlert.getPosition().x - 30 * dir.x, newAlert.getPosition().y - 30 * dir.y);

			Alert alert;
			alert.msprite = newAlert;
			alert.distanceText = dist;

			alertsList.push_back(alert);
		}


		if(!carriersList[cnt].IsStillAlive())
		{
			soundmanager.CreateSound(SoundManager::ENEMYDIED);
			carriersList.erase(carriersList.begin() + cnt);
			--cnt;
		}
		else if(homeplanet.isInFriendlyShieldBubble(carriersList[cnt].GetPosition()))
		{
			homeplanet.ShieldHit(carriersList[cnt].GetPosition(), carriersList[cnt].GetValue());
			carriersList[cnt].health = -1;
		}
		else if(homeplanet.GetHitbox().getGlobalBounds().contains(carriersList[cnt].GetPosition()))
		{
			homeplanet.TakeDamage(1);
			carriersList[cnt].health = -1;
		}
	}


	// DROPSHIP ENEMIES
	for(unsigned int cnt = 0; cnt < dropshipsList.size(); cnt++)
	{
		numOfEnemies++;
		dropshipsList[cnt].Update(_deltaTime);

		if(!wavemanger.isInShieldBubble(dropshipsList[cnt].msprite.getPosition()))
		{
			if(dropshipsList[cnt].msprite.getGlobalBounds().intersects(player.GetGlobalBounds()))
			{
				player.ResetPlayer();
				dropshipsList[cnt].health = -1;

				moneyui.money = std::max(0, (int)(moneyui.money - 2 * dropshipsList[cnt].GetValue()));
			}
		}

		if(!viewRegion.contains(dropshipsList[cnt].msprite.getPosition()))
		{
			sf::Sprite newAlert;
			newAlert.setTexture(alertTexture);

			sf::Vector2f dir(dropshipsList[cnt].msprite.getPosition().x - player.GetPosition().x,
			                 dropshipsList[cnt].msprite.getPosition().y - player.GetPosition().y);
			dir = Normalize(dir);

			newAlert.setRotation(atan2(dir.y, dir.x) * 180 / 3.14159 + 90);
			newAlert.setPosition(mainView.getCenter().x + radius * dir.x,
			                     mainView.getCenter().y + radius * dir.y);

			sf::Text dist;
			dist.setFont(subFont);
			dist.setColor(sf::Color::Red);
			dist.setCharacterSize(20);
			dist.setString(std::to_string((int)DistanceBetween(dropshipsList[cnt].msprite.getPosition(), player.GetPosition())));
			dist.setPosition(newAlert.getPosition().x - 30 * dir.x, newAlert.getPosition().y - 30 * dir.y);

			Alert alert;
			alert.msprite = newAlert;
			alert.distanceText = dist;

			alertsList.push_back(alert);
		}


		if(!dropshipsList[cnt].IsStillAlive())
		{
			soundmanager.CreateSound(SoundManager::ENEMYDIED);
			dropshipsList.erase(dropshipsList.begin() + cnt);
			--cnt;
		}
		else if(homeplanet.isInFriendlyShieldBubble(dropshipsList[cnt].GetPosition()))
		{
			homeplanet.ShieldHit(dropshipsList[cnt].GetPosition(), dropshipsList[cnt].GetValue());
			dropshipsList[cnt].health = -1;
		}
		else if(homeplanet.GetHitbox().getGlobalBounds().contains(dropshipsList[cnt].GetPosition()))
		{
			homeplanet.TakeDamage(1);
			dropshipsList[cnt].health = -1;
		}
	}


	if(numOfEnemies > 0)
		justCompletedWave = true;

	if(numOfEnemies == 0)
	{
		if(justCompletedWave)
		{
			soundmanager.CreateSound(SoundManager::PLANETRECOVERED);
			soundmanager.CreateSound(SoundManager::LEVELCOMPLETE);
			homeplanet.TakeDamage(-1);
			justCompletedWave = false;
		}

		timeUntilNextWaveStarts -= _deltaTime;

		if(timeUntilNextWaveStarts.asSeconds() <= 0)
			timeUntilNextWaveStarts = sf::Time::Zero;

		sprintf(nextWaveString, "NEXT WAVE IN %.2f SECONDS", timeUntilNextWaveStarts.asSeconds());

		timeUntilNextWaveText.setString(nextWaveString);
		timeUntilNextWaveText.setOrigin(timeUntilNextWaveText.getGlobalBounds().width / 2, 0.f);
		timeUntilNextWaveText.setPosition(mainView.getCenter().x, mainView.getCenter().y - mainView.getSize().y / 3);
	}
	if(timeUntilNextWaveStarts.asSeconds() <= 0)
	{
		timeUntilNextWaveStarts = sf::seconds(10);
		moneyui.waveNumber++;
		SpawnNextWave();
	}

}

void WaveManger::Draw(sf::RenderWindow& _window)
{
	// TODO: Loop through each vector and draw everything
	for(unsigned int cnt = 0; cnt < carriersList.size(); cnt++)
		carriersList[cnt].Draw(_window);

	for(unsigned int cnt = 0; cnt < supportList.size(); cnt++)
		supportList[cnt].Draw(_window);

	for(unsigned int cnt = 0; cnt < armouredList.size(); cnt++)
		armouredList[cnt].Draw(_window);

	for(unsigned int cnt = 0; cnt < scoutsList.size(); cnt++)
		scoutsList[cnt].Draw(_window);

	for(unsigned int cnt = 0; cnt < dropshipsList.size(); cnt++)
		dropshipsList[cnt].Draw(_window);

	if(numOfEnemies == 0)
		_window.draw(timeUntilNextWaveText);
}

sf::Vector2f WaveManger::GetValidEnemySpawn()
{
	sf::Vector2f ret((rand() % 3500 + 1) * pow(-1, rand() % 2), (rand() % 3500 + 1) * pow(-1, rand() % 2));

	while(DistanceBetween(ret, homeplanet.GetPosition()) <= 1100)
		ret = sf::Vector2f((rand() % 3500 + 1) * pow(-1, rand() % 2), (rand() % 3500 + 1) * pow(-1, rand() % 2));

	return ret;
}

unsigned int WaveManger::GetNumEnemies()
{
	return numOfEnemies;
}

void WaveManger::DrawAlerts(sf::RenderWindow& _window)
{
	for(unsigned int cnt = 0; cnt < alertsList.size(); cnt++)
	{
		_window.draw(alertsList[cnt].msprite);
		_window.draw(alertsList[cnt].distanceText);
	}
}

sf::Vector2f WaveManger::GetClosestEnemyLocation(sf::Vector2f _start)
{
	sf::Vector2f ret;
	float dist = 1000000000;
	if(carriersList.size() > 0)
	{
		dist = std::min(dist, DistanceBetween(carriersList[0].msprite.getPosition(), _start));
		ret = carriersList[0].msprite.getPosition();
		for(unsigned int cnt = 0; cnt < carriersList.size(); cnt++)
		{
			if(DistanceBetween(carriersList[cnt].msprite.getPosition(), _start) < dist)
			{
				dist = DistanceBetween(carriersList[cnt].msprite.getPosition(), _start);
				ret = carriersList[cnt].msprite.getPosition();
			}
		}
	}
	if(supportList.size() > 0)
	{
		dist = std::min(dist, DistanceBetween(supportList[0].msprite.getPosition(), _start));
		ret = supportList[0].msprite.getPosition();
		for(unsigned int cnt = 0; cnt < supportList.size(); cnt++)
		{
			if(DistanceBetween(supportList[cnt].msprite.getPosition(), _start) < dist)
			{
				dist = DistanceBetween(supportList[cnt].msprite.getPosition(), _start);
				ret = supportList[cnt].msprite.getPosition();
			}
		}
	}
	if(armouredList.size() > 0)
	{
		dist = std::min(dist, DistanceBetween(armouredList[0].msprite.getPosition(), _start));
		ret = armouredList[0].msprite.getPosition();
		for(unsigned int cnt = 0; cnt < armouredList.size(); cnt++)
		{
			if(DistanceBetween(armouredList[cnt].msprite.getPosition(), _start) < dist)
			{
				dist = DistanceBetween(armouredList[cnt].msprite.getPosition(), _start);
				ret = armouredList[cnt].msprite.getPosition();
			}
		}
	}
	if(scoutsList.size() > 0)
	{
		dist = std::min(dist, DistanceBetween(scoutsList[0].msprite.getPosition(), _start));
		ret = scoutsList[0].msprite.getPosition();
		for(unsigned int cnt = 0; cnt < scoutsList.size(); cnt++)
		{
			if(DistanceBetween(scoutsList[cnt].msprite.getPosition(), _start) < dist)
			{
				dist = DistanceBetween(scoutsList[cnt].msprite.getPosition(), _start);
				ret = scoutsList[cnt].msprite.getPosition();
			}
		}
	}
	// Perhaps don't include dropships in this category ??
	if(dropshipsList.size() > 0)
	{
		dist = std::min(dist, DistanceBetween(dropshipsList[0].msprite.getPosition(), _start));
		ret = dropshipsList[0].msprite.getPosition();
		for(unsigned int cnt = 0; cnt < dropshipsList.size(); cnt++)
		{
			if(DistanceBetween(dropshipsList[cnt].msprite.getPosition(), _start) < dist)
			{
				dist = DistanceBetween(dropshipsList[cnt].msprite.getPosition(), _start);
				ret = dropshipsList[cnt].msprite.getPosition();
			}
		}
	}

	return ret;
}

bool WaveManger::isInShieldBubble(sf::Vector2f _point)
{
	for(unsigned int cnt = 0; cnt < supportList.size(); cnt++)
	{
		if(supportList[cnt].shieldBubbleHitbox.getGlobalBounds().contains(_point))
			return true;
	}
	return false;
}

void WaveManger::CarrierSpawnDropship(sf::Vector2f _pos)
{
	EnemyDropship drop;
	drop.Init(_pos);
	dropshipsList.push_back(drop);
}

bool WaveManger::didCarriersSpawn()
{
	return carriersList.size() > 0;
}

sf::Vector2f WaveManger::GetClosestCarrier(sf::Vector2f _start)
{
	sf::Vector2f ret;
	float dist = 100000;
	if(carriersList.size() > 0)
	{
		dist = std::min(dist, DistanceBetween(carriersList[0].msprite.getPosition(), _start));
		ret = carriersList[0].msprite.getPosition();
		for(unsigned int cnt = 1; cnt < carriersList.size(); cnt++)
		{
			if(DistanceBetween(carriersList[cnt].msprite.getPosition(), _start) < dist)
			{
				dist = DistanceBetween(carriersList[cnt].msprite.getPosition(), _start);
				ret = carriersList[cnt].msprite.getPosition();
			}
		}

		return ret;
	}
	return homeplanet.GetPosition();
}

sf::Vector2f WaveManger::GetValidLargeEnemySpawn(bool _isCarrier)
{
	sf::Vector2f ret((rand() % 4500 + 1) * pow(-1, rand() % 2), (rand() % 4500 + 1) * pow(-1, rand() % 2));

	if(_isCarrier)
	{
		while(DistanceBetween(ret, homeplanet.GetPosition()) <= 2000)
			ret = sf::Vector2f((rand() % 4500 + 1) * pow(-1, rand() % 2), (rand() % 4500 + 1) * pow(-1, rand() % 2));
	}
	else
	{
		int numLoops = 0;
		if(numLoops > 100)
			return GetValidLargeEnemySpawn(true);

		while(DistanceBetween(ret, homeplanet.GetPosition()) <= 2000 || (didCarriersSpawn() && DistanceBetween(GetClosestCarrier(ret), ret) > 800)
		        || GetNumEnemiesInRange(ret, 2000, 3) > 3)
		{
			ret = sf::Vector2f((rand() % 4500 + 1) * pow(-1, rand() % 2), (rand() % 4500 + 1) * pow(-1, rand() % 2));
			++numLoops;
		}
	}


	return ret;
}

int WaveManger::GetNumEnemiesInRange(sf::Vector2f _start, float _radius, int _minClass)
{
	int numEnemies = 0;


	if(_minClass  <= 1)
	{
		for(unsigned int cnt = 0; cnt < scoutsList.size(); cnt++)
		{
			if(DistanceBetween(scoutsList[cnt].msprite.getPosition(), _start) < _radius)
			{
				++numEnemies;
			}
		}
	}


	if(_minClass  <= 2)
	{
		for(unsigned int cnt = 0; cnt < armouredList.size(); cnt++)
		{
			if(DistanceBetween(armouredList[cnt].msprite.getPosition(), _start) < _radius)
			{
				++numEnemies;
			}
		}
	}


	if(_minClass  <= 3)
	{
		for(unsigned int cnt = 1; cnt < supportList.size(); cnt++)
		{
			if(DistanceBetween(supportList[cnt].msprite.getPosition(), _start) < _radius)
			{
				++numEnemies;
			}
		}
	}

	if(_minClass  <= 4)
	{
		for(unsigned int cnt = 1; cnt < carriersList.size(); cnt++)
		{
			if(DistanceBetween(carriersList[cnt].msprite.getPosition(), _start) < _radius)
			{
				++numEnemies;
			}
		}
	}


	return numEnemies;
}

void WaveManger::Reset()
{
	scoutsList.clear();
	armouredList.clear();
	supportList.clear();
	carriersList.clear();
	dropshipsList.clear();
}
