#ifndef ENEMYSUPPORT_H
#define ENEMYSUPPORT_H

#include <SFML/Graphics.hpp>

#include "Enemy.h"

class EnemySupport : public Enemy
{
	int shieldHealth;
	sf::Sprite shieldBubble;
	sf::Time bubbleRegenTimer;
	sf::Time bubbleExplosionDamageTimer;
	int stage;
	sf::Vector2f myOffset;
	
	
	float distToMyCarrier;
	
public:
	EnemySupport();
	~EnemySupport();
	
	sf::CircleShape shieldBubbleHitbox;
	
	void Init(sf::Vector2f _spawn);
	void Update(sf::Time _deltaTime);
	void Draw(sf::RenderWindow& _window);
};

#endif // ENEMYSUPPORT_H
