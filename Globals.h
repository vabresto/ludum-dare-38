#ifndef GLOBALS_H
#define GLOBALS_H

#include <SFML/Audio.hpp>

#include "GameObject.h"
#include "WaveManger.h"
#include "SoundManager.h"
#include "MiscUI.h"
#include "Player.h"
#include "Planet.h"
#include "MoneyUI.h"
#include "ParticleExhaust.h"
#include "BuyBar.h"
#include "Bullet.h"
#include "Missile.h"
#include "Explosion.h"

extern unsigned int highscore, score;
extern sf::RenderWindow window;
extern sf::View mainView;

extern MiscUI miscui;

extern std::vector<GameObject*> objectsList;
extern std::vector<ParticleExhaust> exhPartsList;
extern std::vector<Bullet> bulletsList;
extern std::vector<Missile> missilesList;
extern std::vector<Explosion> explosionsList;

extern sf::Texture planetsTexture;
extern sf::Texture itemsTexture;
extern sf::Texture exhaustGoodTexture;
extern sf::Texture exhaustEnemyTexture;
extern sf::Texture buildingsTexture;
extern sf::Texture bulletsTexture;
extern sf::Texture missilesTexture;
extern sf::Texture progressBarCoverTexture;
extern sf::Texture progressBarTexture;
extern sf::Texture alertTexture;
extern sf::Texture explosionTexture;
extern sf::Texture pauseOverlayTexture;
extern sf::Texture energyBarCoverTexture;
extern sf::Texture energyBarTexture;
extern sf::Texture shieldBubbleTexture;

extern sf::Texture scoutEnemyTexture;
extern sf::Texture armouredEnemyTexture;
extern sf::Texture supportEnemyTexture;
extern sf::Texture supportEnemyShieldTexture;
extern sf::Texture carrierEnemyTexture;
extern sf::Texture dropshipEnemyTexture;

extern sf::Font mainFont;
extern sf::Font subFont;


extern WaveManger wavemanger;
extern Player player;
extern Planet homeplanet;
extern BuyBar buybar;
extern MoneyUI moneyui;
extern SoundManager soundmanager;

extern sf::Clock gameClock;
extern const sf::Time timePerFrame;
extern sf::Time timeAcc;

extern sf::Vector2f MousePos;



extern sf::SoundBuffer gameoverBuffer;
extern sf::SoundBuffer levelCompleteBuffer;
extern sf::SoundBuffer levelComplete2Buffer;
extern sf::SoundBuffer startBuildingBuffer;
extern sf::SoundBuffer doneBuildingBuffer;
extern sf::SoundBuffer planetHitBuffer;
extern sf::SoundBuffer planetRecoveredBuffer;
extern sf::SoundBuffer alliedShieldRegenBuffer;
extern sf::SoundBuffer enemyShieldRegenBuffer;
extern sf::SoundBuffer missileExplodeBuffer;
extern sf::SoundBuffer shoot1Buffer;
extern sf::SoundBuffer shoot2Buffer;
extern sf::SoundBuffer powerdownBuffer;
extern sf::SoundBuffer enemyDiedBuffer;
extern sf::SoundBuffer mouseoverBuffer;
extern sf::SoundBuffer hitEnemyBuffer;
extern sf::SoundBuffer hitEnemyShieldBuffer;




float DistanceBetween(sf::Vector2f _a, sf::Vector2f _b);
sf::Vector2f Normalize(sf::Vector2f _a);
sf::Vector2f RotationToDirection(float _rot);
#endif
