#ifndef MISCUI_H
#define MISCUI_H

#include <SFML/Graphics.hpp>
#include <SFML/Audio.hpp>

class MiscUI
{
	sf::Text pausedText;
	sf::Text gameOverText;
	sf::Text gameOverText2;
	
	sf::Sprite pauseOverlay;
	sf::Sound gameOverSound;
	
	bool PlayedGameOverSound;
public:
	bool isPaused;
	bool isGameOver;
	sf::Time timeSincePauseToggled;


	MiscUI();
	~MiscUI();
	
	void Init();
	void Update(sf::Time _deltaTime);
	void Draw(sf::RenderWindow& _window);
	
	void RestartGame();
};

#endif // MISCUI_H
