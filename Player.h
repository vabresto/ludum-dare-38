#ifndef PLAYER_H
#define PLAYER_H

#include <SFML/Graphics.hpp>

#include "GameObject.h"
#include "Item.h"

class Player : public GameObject
{
	sf::Texture mtexture;
	sf::Time fireCooldown;
	sf::Time timeSinceLastFired;
	
public:
	Player();
	~Player();
	
	sf::Vector2f mvelocity;
	float mMaxSpeed;
	float mDeltaSpeed;
	std::vector<Item> items;
	
	
	sf::Vector2f GetExhaustPlacement();
	void Init(std::string _textFileName);
	void Update(sf::Time _deltaTime);
	
	void ResetPlayer();
};

#endif // PLAYER_H
