#ifndef MISSILE_H
#define MISSILE_H

#include <SFML/Graphics.hpp>
#include "GameObject.h"



class Missile : public GameObject
{
	float moveSpeed;
	bool hasTarget;
	float theta;
	float radius;
	float prevDist;
	
	sf::Time timeToLive;

public:
	Missile();
	~Missile();

	void Init(sf::Vector2f _spawn, float _ms, float _radius, float _theta);
	void Fire();
	void Update(sf::Time _deltaTime);
	void Draw(sf::RenderWindow& _window);
	bool IsStillAlive();
};

#endif // MISSILE_H
