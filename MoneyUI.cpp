#include <iostream>
#include <string>

#include "MoneyUI.h"
#include "Globals.h"
#include "WaveManger.h"

MoneyUI::MoneyUI()
{
}

MoneyUI::~MoneyUI()
{
}

void MoneyUI::Init()
{
	if(!moneyUITexture.loadFromFile("assets/moneyui.png"))
		std::cout << "[ERROR]: Could not load texture assets/moneyui.png" << std::endl;

	msprite.setTexture(moneyUITexture);
	msprite.setOrigin(256.f, 0.f);

	waveText.setFont(mainFont);
	waveText.setString("Wave: 00");
	waveText.setCharacterSize(22);

	waveNumber = 0;

	moneyText.setFont(mainFont);
	moneyText.setString("$ 000000");
	moneyText.setCharacterSize(22);

	money = 400;

	enemiesCounterSprite.setTexture(moneyUITexture);
	enemiesCounterSprite.setTextureRect(sf::IntRect(256.f, 0.f, -256.f, 64.f));

	enemiesLeftText.setFont(mainFont);
	enemiesLeftText.setString("Enemies Left:\n00000");
	enemiesLeftText.setCharacterSize(22);

	energyPositive = true;


	energyBarCover.setTexture(energyBarCoverTexture);
	energyBarCover.setOrigin(128.f, 0.f);
	energyBarCover.setScale(3.75, 1.f);

	energyBar.setTexture(energyBarTexture);
	energyBar.setScale(energyBarCover.getScale());
	energyBar.setOrigin(energyBarCover.getOrigin());


	//sf::Text energyText;
	energyText.setFont(subFont);
	energyText.setCharacterSize(20);

	numMissilesArmedText.setFont(subFont);
	numMissilesArmedText.setCharacterSize(20);

	//sf::Text energyRegenText;
}

void MoneyUI::Update(sf::Time _deltaTime)
{
	msprite.setPosition(mainView.getCenter().x + mainView.getSize().x / 2, mainView.getCenter().y - mainView.getSize().y / 2);


	waveText.setString("Wave: " + std::to_string(waveNumber));
	moneyText.setString("$ " + std::to_string(money));
	enemiesLeftText.setString("Enemies Left:\n" + std::to_string(wavemanger.GetNumEnemies()));

	waveText.setPosition(msprite.getPosition().x - 180.f, msprite.getPosition().y + 3);
	moneyText.setPosition(msprite.getPosition().x - 180.f, msprite.getPosition().y + 33);

	enemiesCounterSprite.setPosition(mainView.getCenter().x - mainView.getSize().x / 2, mainView.getCenter().y - mainView.getSize().y / 2);
	enemiesLeftText.setPosition(enemiesCounterSprite.getPosition().x + 20.f, enemiesCounterSprite.getPosition().y + 3);


	//energyBar.setPosition(mainView.getCenter().x - mainView.getSize().x/2 + 10, mainView.getCenter().y - mainView.getSize().y/4);
	//energyBar.setScale(mainView.getSize().x / 200, mainView.getSize().y / 600);
	energyBar.setPosition(buybar.GetPosition().x - 64 + 304 , buybar.GetPosition().y - 32 - 128);

	energyBarCover.setPosition(energyBar.getPosition());
	energyBarCover.setScale(energyBar.getScale().x * (1 - homeplanet.GetEnergy() / homeplanet.GetEnergyCap()), energyBar.getScale().y);

	//energyText.setPosition(energyBar.getPosition().x, );

	sprintf(energyTextString, "%.0f / %.0f\t(%0.f)", homeplanet.GetEnergy(), homeplanet.GetEnergyCap(), homeplanet.GetEnergyRegenRate());
	energyText.setString(energyTextString);
	energyText.setOrigin(energyText.getGlobalBounds().width / 2, energyText.getGlobalBounds().height / 2);
	energyText.setPosition(mainView.getCenter().x, energyBar.getPosition().y + 10);

	if(energyPositive && homeplanet.GetEnergyRegenRate() < 0)
	{
		energyText.setColor(sf::Color::Red);
		energyPositive = false;
	}
	else if(!energyPositive && homeplanet.GetEnergyRegenRate() >= 0)
	{
		energyText.setColor(sf::Color::White);
		energyPositive = true;
	}

	sprintf(numMissilesArmedString, "Missiles Armed: %d", homeplanet.ArmedMissiles());
	numMissilesArmedText.setString(numMissilesArmedString);
	numMissilesArmedText.setOrigin(numMissilesArmedText.getGlobalBounds().width / 2, numMissilesArmedText.getGlobalBounds().height / 2);
	numMissilesArmedText.setPosition(mainView.getCenter().x, buybar.GetPosition().y - 32 - 128 - 15);

}

void MoneyUI::Draw(sf::RenderWindow& _window)
{
	_window.draw(msprite);
	_window.draw(moneyText);
	_window.draw(waveText);

	_window.draw(enemiesCounterSprite);
	_window.draw(enemiesLeftText);

	_window.draw(energyBar);
	_window.draw(energyBarCover);

	_window.draw(energyText);
	_window.draw(numMissilesArmedText);

}

void MoneyUI::ResetMoneyUI()
{
	waveNumber = 0;
	money = 400;
}
