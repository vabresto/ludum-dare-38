#include <iostream>
#include <random>

#include "EnemySupport.h"
#include "Globals.h"

EnemySupport::EnemySupport()
{
}

EnemySupport::~EnemySupport()
{
}

void EnemySupport::Init(sf::Vector2f _spawn)
{
	msprite.setTexture(supportEnemyTexture);
	msprite.setOrigin(48.f, 48.f);

	msprite.setPosition(_spawn);

	moveTarget = homeplanet.GetPosition();
	moveSpeed = 4.f;
	health = 1;

	length = 12;

	moneyValue = 150;

	shieldHealth = 5;
	shieldBubble.setTexture(supportEnemyShieldTexture);
	shieldBubble.setOrigin(32.f, 32.f);
	shieldBubble.setScale(shieldHealth * 2, shieldHealth * 2);
	shieldBubble.setColor(sf::Color(255, 255, 255, 100));


	bubbleRegenTimer = sf::Time::Zero;
	bubbleExplosionDamageTimer = sf::Time::Zero;


	shieldBubbleHitbox.setRadius(300);
	shieldBubbleHitbox.setOrigin(300.f, 300.f);
	shieldBubbleHitbox.setOutlineColor(sf::Color::Blue);
	shieldBubbleHitbox.setFillColor(sf::Color::Transparent);
	shieldBubbleHitbox.setOutlineThickness(2);



	if(wavemanger.didCarriersSpawn())
	{
		stage = 0;
		distToMyCarrier = DistanceBetween(msprite.getPosition(), wavemanger.GetClosestCarrier(msprite.getPosition()));

		myOffset.x = (rand() % 50 + 25) * pow(-1, rand() % 2);
		myOffset.y = (rand() % 50 + 25) * pow(-1, rand() % 2);
	}
	else
	{
		stage = 2;
	}
}

void EnemySupport::Update(sf::Time _deltaTime)
{
	if(wavemanger.didCarriersSpawn())
	{
		stage = 0;
		distToMyCarrier = DistanceBetween(msprite.getPosition(), wavemanger.GetClosestCarrier(msprite.getPosition()));
	}
	else
	{
		stage = 2;
	}


	bubbleExplosionDamageTimer += _deltaTime;
	timeSinceHitByExplosion += _deltaTime;
	isDamaged = false;

	if(shieldHealth > 0)
	{
		shieldBubble.setPosition(msprite.getPosition());
		shieldBubbleHitbox.setPosition(msprite.getPosition());
	}
	if(shieldHealth < 5)
	{
		bubbleRegenTimer += _deltaTime;
		if(bubbleRegenTimer.asSeconds() > 5)
		{
			shieldHealth ++;
			soundmanager.CreateSound(SoundManager::ENEMYSHIELDREGEN);
			bubbleRegenTimer = sf::seconds(4);

			shieldBubble.setScale(shieldHealth * 2, shieldHealth * 2);
			shieldBubbleHitbox.setRadius(30 * 2 * shieldHealth);
			shieldBubbleHitbox.setOrigin(30 * 2 * shieldHealth, 30 * 2 * shieldHealth);
		}
	}

	if(damagedTimer.asSeconds() > 0)
	{
		damagedTimer -= _deltaTime;
	}

	moveTarget = wavemanger.GetClosestCarrier(msprite.getPosition());
	moveTarget.x += myOffset.x;
	moveTarget.y += myOffset.y;

	distToMyCarrier = DistanceBetween(msprite.getPosition(), moveTarget);

	if(stage == 0)
	{
		moveSpeed = 4.f;
		if(distToMyCarrier < 100)
		{
			//std::cout << "Beginning escort" << std::endl;
			stage = 1;
		}
	}
	if(stage == 1)
	{
		moveSpeed = 3.f;
		if(wavemanger.didCarriersSpawn() && distToMyCarrier < 800)
		{
			stage = 0;
		}
		else
		{
			//std::cout << "Finished escorting" << std::endl;
			//moveTarget = homeplanet.GetPosition();
			stage = 2;
		}
	}
	if(stage == 2)
	{
		moveSpeed = 4.f;
		moveTarget = homeplanet.GetPosition();
	}

	sf::Vector2f dir = moveTarget;
	dir.x -= msprite.getPosition().x;
	dir.y -= msprite.getPosition().y;

	dir = Normalize(dir);
	dir.x *= moveSpeed;
	dir.y *= moveSpeed;

	msprite.setRotation(atan2(dir.y, dir.x) * 180 / 3.14159 + 90);
	msprite.setPosition(msprite.getPosition().x + dir.x, msprite.getPosition().y + dir.y);

	if(rand() % 7 < 5/* && exhPartsList.size() < 100*/)
	{
		ParticleExhaust exh;
		exh.Init(sf::Vector2f(msprite.getPosition().x - dir.x * length,
		                      msprite.getPosition().y - dir.y * length),
		         msprite.getRotation(), 1.25, 1);
		exhPartsList.push_back(exh);
	}

	if(!wavemanger.isInShieldBubble(msprite.getPosition()))
	{
		for(unsigned int cnt = 0; cnt < bulletsList.size(); cnt++)
		{
			if(msprite.getGlobalBounds().contains(bulletsList[cnt].msprite.getPosition()))
			{
				bulletsList[cnt].HitSomething();
				health--;
				soundmanager.CreateSound(SoundManager::ENEMYHIT);
				isDamaged = true;

				if(health <= 0)
				{
					moneyui.money += moneyValue;
				}
			}
		}
		for(unsigned int cnt = 0; cnt < explosionsList.size(); cnt++)
		{
			if(timeSinceHitByExplosion.asSeconds() > 0.5 && msprite.getGlobalBounds().intersects(explosionsList[cnt].GetGlobalBounds()))
			{
				timeSinceHitByExplosion = sf::Time::Zero;
				health --;
				soundmanager.CreateSound(SoundManager::ENEMYHIT);
				isDamaged = true;
				if(health <= 0)
				{
					moneyui.money += moneyValue;
				}
			}
		}
	}

	if(shieldHealth > 0)
	{
		for(unsigned int cnt = 0; cnt < bulletsList.size(); cnt++)
		{
			if(shieldBubbleHitbox.getGlobalBounds().contains(bulletsList[cnt].msprite.getPosition()))
			{
				bulletsList[cnt].HitSomething();
				shieldHealth--;
				soundmanager.CreateSound(SoundManager::ENEMYSHIELDHIT);
				
				bubbleRegenTimer = sf::Time::Zero;

				shieldBubble.setScale(shieldHealth * 2, shieldHealth * 2);
				shieldBubbleHitbox.setRadius(30 * 2 * shieldHealth);
				shieldBubbleHitbox.setOrigin(30 * 2 * shieldHealth, 30 * 2 * shieldHealth);
			}
		}

		for(unsigned int cnt = 0; cnt < explosionsList.size(); cnt++)
		{
			if(bubbleExplosionDamageTimer.asSeconds() > 0.5 && shieldBubbleHitbox.getGlobalBounds().intersects(explosionsList[cnt].GetGlobalBounds()))
			{
				shieldHealth--;
				soundmanager.CreateSound(SoundManager::ENEMYSHIELDHIT);
				bubbleRegenTimer = sf::Time::Zero;
				bubbleExplosionDamageTimer = sf::Time::Zero;

				shieldBubble.setScale(shieldHealth * 2, shieldHealth * 2);
				shieldBubbleHitbox.setRadius(30 * 2 * shieldHealth);
				shieldBubbleHitbox.setOrigin(30 * 2 * shieldHealth, 30 * 2 * shieldHealth);
			}
		}
	}


	if(isDamaged)
	{
		damagedTimer = sf::seconds(0.5);
		msprite.setColor(sf::Color::Cyan);
	}
	else if(damagedTimer.asSeconds() <= 0)
	{
		msprite.setColor(sf::Color::White);
	}
}

void EnemySupport::Draw(sf::RenderWindow& _window)
{
	_window.draw(msprite);
	if(shieldHealth > 0)
	{
		_window.draw(shieldBubble);
		//_window.draw(shieldBubbleHitbox);
	}

}
