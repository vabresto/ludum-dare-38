#ifndef ENEMYARMOURED_H
#define ENEMYARMOURED_H

#include <SFML/Graphics.hpp>

#include "Enemy.h"


class EnemyArmoured : public Enemy
{
public:
	EnemyArmoured();
	~EnemyArmoured();

	void Init(sf::Vector2f _spawn);
};

#endif // ENEMYARMOURED_H
