#include "Explosion.h"
#include "Globals.h"

Explosion::Explosion()
{
	soundmanager.CreateSound(SoundManager::MISSILEEXPLODE);
}

Explosion::~Explosion()
{
}

void Explosion::Init(sf::Vector2f _center)
{
	msprite.setTexture(explosionTexture);
	msprite.setTextureRect(sf::IntRect(0.f, 0.f, 32.f, 32.f));
	msprite.setOrigin(16.f, 16.f);
	msprite.setPosition(_center);

	timeToLive = sf::seconds(2.4);
	stage = 0;
}

void Explosion::Update(sf::Time _deltaTime)
{
	timeToLive -= _deltaTime;

	if(stage == 1 && timeToLive.asSeconds() < 1)
	{
		msprite.setTextureRect(sf::IntRect(0.f, 64.f, 32.f, 32.f));
		stage = 2;
		msprite.setScale(3.f, 3.f);
	}
	else if(stage == 0 && timeToLive.asSeconds() < 1.8)
	{
		msprite.setTextureRect(sf::IntRect(0.f, 32.f, 32.f, 32.f));
		stage = 1;
		msprite.setScale(2.f, 2.f);
	}

}

bool Explosion::IsStillAlive()
{
	return timeToLive.asSeconds() > 0;
}
