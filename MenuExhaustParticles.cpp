#include "MenuExhaustParticles.h"

MenuExhaustParticles::MenuExhaustParticles()
{
}

MenuExhaustParticles::~MenuExhaustParticles()
{
}

void MenuExhaustParticles::Draw(sf::RenderWindow& _window)
{
	_window.draw(msprite);
}
void MenuExhaustParticles::Init(sf::Texture& _texture, sf::Vector2f _pos, sf::Time _ttl)
{
	msprite.setTexture(_texture);
	msprite.setColor(sf::Color(255,255,255,100));
	msprite.setPosition(_pos.x -30, _pos.y - 75);
	
	timeToLive = _ttl;
}
bool MenuExhaustParticles::IsStillAlive()
{
	return timeToLive.asSeconds() > 0;
}
void MenuExhaustParticles::Update(sf::Time _deltaTime)
{
	timeToLive -= _deltaTime;
	msprite.move(5, -3);
	msprite.setColor(sf::Color(255,255,255,100 * (timeToLive.asSeconds() / 1.5)));
}
