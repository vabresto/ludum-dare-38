#ifndef MENUEXHAUSTPARTICLES_H
#define MENUEXHAUSTPARTICLES_H


#include "GameObject.h"

class MenuExhaustParticles : public GameObject
{
public:
sf::Time timeToLive;
	MenuExhaustParticles();
	~MenuExhaustParticles();

public:

	void Draw(sf::RenderWindow& _window);

	void Init(sf::Texture& _texture, sf::Vector2f _pos, sf::Time _ttl);

	bool IsStillAlive();

	void Update(sf::Time _deltaTime);
};

#endif // MENUEXHAUSTPARTICLES_H
