#include "Building.h"
#include "Globals.h"
#include <random>
#include <math.h>
#include <iostream>

#include "Building.h"
#include "Globals.h"

Building::Building()
{
}

Building::~Building()
{
}

int Building::currentMissileLauncher = 0;
int Building::numMissileLaunchers = 0;
sf::Time Building::timeSinceLastMissileFired;

void Building::Init(StructureType _type)
{
	structtype = _type;
	//offset = sf::Vector2f(rand() % 64 * pow(-1, rand() % 2), rand() % 64 * pow(-1, rand() % 2));

	msprite.setOrigin(16.f, 32.f);
	msprite.setTexture(buildingsTexture);

	switch(structtype)
	{
	case CANNON:
	{
		energyUsage = 20;
		msprite.setTextureRect(sf::IntRect(0, 0, 32, 32));
		fireCooldown = sf::seconds(0.125);
		timeSinceLastFired = sf::Time::Zero;
		break;
	}
	case SOLARPANEL:
	{
		msprite.setTextureRect(sf::IntRect(0, 32, 32, 32));
		energyUsage = -150;
		homeplanet.energyRegenRate -= energyUsage;
		break;
	}
	case MISSILELAUNCHER:
	{
		msprite.setTextureRect(sf::IntRect(0, 64, 32, 32));
		fireCooldown = sf::seconds(5);
		timeSinceLastFired = sf::seconds(2);
		isArmed = false;
		Missile mmissile;

		missileLauncherId = numMissileLaunchers;
		numMissileLaunchers ++;


		break;
	}
	case SHIELDGENERATOR:
	{
		msprite.setTextureRect(sf::IntRect(0, 96, 32, 32));
		msprite.setOrigin(16.f, 16.f);
		energyUsage = 50;
		energyCapModifier = 500;
		homeplanet.energyRegenRate -= energyUsage;

		shieldBubble.setTexture(shieldBubbleTexture);
		shieldBubble.setOrigin(32.f, 32.f);
		shieldBubble.setScale(2.f, 2.f);
		shieldBubble.setColor(sf::Color(255, 255, 255, 100));


		hitbox.setRadius(64.f);
		hitbox.setOrigin(64.f, 64.f);
		hitbox.setOutlineColor(sf::Color::Red);
		hitbox.setFillColor(sf::Color::Transparent);
		hitbox.setOutlineThickness(2);
		break;
	}
	case BATTERY:
	{
		msprite.setTextureRect(sf::IntRect(0, 128, 32, 32));
		energyCapModifier = 1250;
		homeplanet.energyCap += energyCapModifier;
		break;
	}
	default:
	{
		break;
	}
	}

	float max = 0;
	if(structtype == Building::SHIELDGENERATOR)
	{
		int angle = rand() % 360;
		offset = sf::Vector2f(homeplanet.GetHitbox().getRadius() * cos(angle),
		                      homeplanet.GetHitbox().getRadius() * sin(angle));

	}
	else
	{
		if(homeplanet.buildings.size() > 0)
		{
			while(max < 32.f)
			{
				offset = sf::Vector2f((rand() % 89 + 1) * pow(-1, rand() % 2), (rand() % 89 + 1) * pow(-1, rand() % 2));
				for(unsigned int cnt = 0; cnt < homeplanet.buildings.size(); cnt++)
				{
					if(DistanceBetween(homeplanet.buildings[cnt].offset, offset) > max)
						max = DistanceBetween(homeplanet.buildings[cnt].offset, offset);
				}
			}
		}
		else
		{
			offset = sf::Vector2f((rand() % 89 + 1) * pow(-1, rand() % 2), (rand() % 89 + 1) * pow(-1, rand() % 2));
		}
	}


	theta = atan2(offset.y, offset.x);
	//std::cout << "Theta " << theta << std::endl;
	radius = offset.x / cos(theta);
	//std::cout << "Radius " << radius << std::endl;
}

void Building::Update(sf::Time _deltaTime)
{
	msprite.setPosition(homeplanet.GetPosition().x + radius * cos(homeplanet.GetRotation() / 180 * 3.14159 + theta),
	                    homeplanet.GetPosition().y + radius * sin(homeplanet.GetRotation() / 180 * 3.14159 + theta));

	msprite.setRotation(atan2(msprite.getPosition().y - homeplanet.GetPosition().y,
	                          msprite.getPosition().x - homeplanet.GetPosition().x) * 180 / 3.14159 + 90);

	isShieldActive = false;
	switch(structtype)
	{
	case CANNON:
	{
		timeSinceLastFired += _deltaTime;
		if(sf::Keyboard::isKeyPressed(sf::Keyboard::Space) && timeSinceLastFired >= fireCooldown && homeplanet.GetEnergy() >= energyUsage)
		{
			timeSinceLastFired = sf::Time::Zero;
			soundmanager.CreateSound(SoundManager::SHOOT);
			homeplanet.currentEnergy -= energyUsage;

			Bullet bullet;
			bullet.Init(msprite.getPosition(), RotationToDirection(msprite.getRotation()), 0.0125, sf::seconds(50));
			bulletsList.push_back(bullet);
		}
		break;
	}
	case SOLARPANEL:
	{
		break;
	}
	case SHIELDGENERATOR:
	{
		// TODO : SHIELD GEN
		shieldBubble.setPosition(msprite.getPosition());
		hitbox.setPosition(msprite.getPosition());
		
		if (homeplanet.GetEnergy() > energyCapModifier)
			isShieldActive = true;
		break;
	}
	case BATTERY:
	{
		break;
	}
	case MISSILELAUNCHER:
	{
		timeSinceLastFired += _deltaTime;

		if(currentMissileLauncher >= numMissileLaunchers)
			currentMissileLauncher = 0;

		if(timeSinceLastFired >= fireCooldown)
		{
			if(!isArmed)
			{
				std::cout << "Armed missle!" << std::endl;
				isArmed = true;

				mmissile.Init(msprite.getPosition(), 3, radius, theta);
				//missilesList.push_back(missle);
			}
			else
			{
				mmissile.Update(_deltaTime);

				if(sf::Keyboard::isKeyPressed(sf::Keyboard::Return) && wavemanger.GetNumEnemies() > 0 &&
				        missileLauncherId == currentMissileLauncher && timeSinceLastMissileFired.asSeconds() > 0.15)
				{
					timeSinceLastFired = sf::Time::Zero;
					timeSinceLastMissileFired = sf::Time::Zero;
					isArmed = false;

					missilesList.push_back(mmissile);
					missilesList[missilesList.size() - 1].Fire();

					currentMissileLauncher++;
				}
			}
		}

		break;
	}
	default:
	{
		break;
	}
	}
}

void Building::Draw(sf::RenderWindow& _window)
{
	_window.draw(msprite);
	if(structtype == Building::SHIELDGENERATOR && homeplanet.GetEnergy() > energyCapModifier)
	{
		_window.draw(shieldBubble);
	}

	if(isArmed)
		_window.draw(mmissile.msprite);
}

bool Building::GetIsArmed()
{
	if(structtype == Building::MISSILELAUNCHER)
	{
		return isArmed;
	}
	return false;
}

bool Building::IsInBubbleShield(sf::Vector2f _start)
{
	if(structtype == Building::SHIELDGENERATOR && homeplanet.currentEnergy >= energyCapModifier)
	{
		return hitbox.getGlobalBounds().contains(_start);
	}
	return false;
}
