#include <stdio.h>
#include <vector>
#include <random>
#include <iostream>
#include <SFML/Graphics.hpp>
#include <SFML/Audio.hpp>

#include "GameObject.h"
#include "Globals.h"
#include "ParticleExhaust.h"
#include "Building.h"

#include "MenuExhaustParticles.h"

std::string dots = "../";

bool isOnStartScreen = true;

sf::Texture buttonDefaultTexture;
sf::Texture buttonMouseOverTexture;
sf::Texture textMainTexture;
sf::Texture textSubTexture;
sf::Texture menuExhaustTexture;
sf::Texture menuShipTexture;
sf::Texture planetSplashTexture;


sf::Font playButtonFont;
sf::Text playButtonText;


sf::Sound mouseOverMenuSound;

int main(int argc, char **argv)
{
	if(!isOnStartScreen)
	{
		srand(5);

		if(!exhaustGoodTexture.loadFromFile("assets/exhaust.png"))
			std::cout << "[ERROR]: Could not load texture assets/exhaust.png" << std::endl;

		if(!exhaustEnemyTexture.loadFromFile("assets/enemyexhaust.png"))
			std::cout << "[ERROR]: Could not load texture assets/enemyexhaust.png" << std::endl;

		if(!planetsTexture.loadFromFile("assets/planets.png"))
			std::cout << "[ERROR]: Could not load texture assets/planets.png" << std::endl;

		if(!buildingsTexture.loadFromFile("assets/buildings.png"))
			std::cout << "[ERROR]: Could not load texture assets/buildings.png" << std::endl;

		if(!bulletsTexture.loadFromFile("assets/blasterbolt.png"))
			std::cout << "[ERROR]: Could not load texture assets/blasterbolt.png" << std::endl;

		if(!missilesTexture.loadFromFile("assets/rocketmissile.png"))
			std::cout << "[ERROR]: Could not load texture assets/rocketmissile.png" << std::endl;

		if(!progressBarCoverTexture.loadFromFile("assets/progressbarcover.png"))
			std::cout << "[ERROR]: Could not load texture assets/progressbarcover.png" << std::endl;

		if(!progressBarTexture.loadFromFile("assets/progressbar.png"))
			std::cout << "[ERROR]: Could not load texture assets/progressbar.png" << std::endl;

		if(!mainFont.loadFromFile("assets/Orbitron-Black.ttf"))
			std::cout << "[ERROR]: Could not load font assets/Orbitron-Black.ttf" << std::endl;

		if(!subFont.loadFromFile("assets/Orbitron-Medium.ttf"))
			std::cout << "[ERROR]: Could not load font assets/Orbitron-Medium.ttf" << std::endl;

		if(!alertTexture.loadFromFile("assets/scanneralert.png"))
			std::cout << "[ERROR]: Could not load texture assets/scanneralert.png" << std::endl;

		if(!explosionTexture.loadFromFile("assets/explosion.png"))
			std::cout << "[ERROR]: Could not load texture assets/explosion.png" << std::endl;

		if(!pauseOverlayTexture.loadFromFile("assets/screenfilter.png"))
			std::cout << "[ERROR]: Could not load texture assets/screenfilter.png" << std::endl;

		if(!energyBarCoverTexture.loadFromFile("assets/energybarcover.png"))
			std::cout << "[ERROR]: Could not load texture assets/energybarcover.png" << std::endl;

		if(!energyBarTexture.loadFromFile("assets/energybar.png"))
			std::cout << "[ERROR]: Could not load texture assets/energybar.png" << std::endl;

		if(!shieldBubbleTexture.loadFromFile("assets/playershield.png"))
			std::cout << "[ERROR]: Could not load texture assets/playershield.png" << std::endl;


		//ENEMIES
		if(!scoutEnemyTexture.loadFromFile("assets/enemyscout.png"))
			std::cout << "[ERROR]: Could not load texture assets/enemyscout.png" << std::endl;

		if(!armouredEnemyTexture.loadFromFile("assets/enemyarmoured.png"))
			std::cout << "[ERROR]: Could not load texture assets/enemyarmoured.png" << std::endl;

		if(!supportEnemyTexture.loadFromFile("assets/enemysupport.png"))
			std::cout << "[ERROR]: Could not load texture assets/enemysupport.png" << std::endl;

		if(!supportEnemyShieldTexture.loadFromFile("assets/enemyshield.png"))
			std::cout << "[ERROR]: Could not load texture assets/enemyshield.png" << std::endl;

		if(!carrierEnemyTexture.loadFromFile("assets/enemycarrier.png"))
			std::cout << "[ERROR]: Could not load texture assets/enemycarrier.png" << std::endl;

		if(!dropshipEnemyTexture.loadFromFile("assets/enemydropship.png"))
			std::cout << "[ERROR]: Could not load texture assets/enemydropship.png" << std::endl;


		//Sound Effects
		if(!gameoverBuffer.loadFromFile("assets/gameover.wav"))
			std::cout << "[ERROR]: Could not load sound effect assets/gameover.wav" << std::endl;

		if(!levelCompleteBuffer.loadFromFile("assets/levelcomplete.wav"))
			std::cout << "[ERROR]: Could not load sound effect ../assets/levelcomplete.wav" << std::endl;

		if(!levelComplete2Buffer.loadFromFile("assets/levelcomplete2.wav"))
			std::cout << "[ERROR]: Could not load sound effect assets/levelcomplete2.wav" << std::endl;

		if(!startBuildingBuffer.loadFromFile("assets/startbuilding.wav"))
			std::cout << "[ERROR]: Could not load sound effect assets/startbuilding.wav" << std::endl;

		if(!doneBuildingBuffer.loadFromFile("assets/donebuilding.wav"))
			std::cout << "[ERROR]: Could not load sound effect assets/donebuilding.wav" << std::endl;

		if(!planetHitBuffer.loadFromFile("assets/planethit.wav"))
			std::cout << "[ERROR]: Could not load sound effect assets/planethit.wav" << std::endl;

		if(!planetRecoveredBuffer.loadFromFile("assets/planetrecovered.wav"))
			std::cout << "[ERROR]: Could not load sound effect assets/planetrecovered.wav" << std::endl;

		if(!alliedShieldRegenBuffer.loadFromFile("assets/alliedshieldregen.wav"))
			std::cout << "[ERROR]: Could not load sound effect assets/alliedshieldregen.wav" << std::endl;

		if(!enemyShieldRegenBuffer.loadFromFile("assets/enemyshieldregen.wav"))
			std::cout << "[ERROR]: Could not load sound effect assets/enemyshieldregen.wav" << std::endl;

		if(!missileExplodeBuffer.loadFromFile("assets/explode.wav"))
			std::cout << "[ERROR]: Could not load sound effect assets/explode.wav" << std::endl;

		if(!shoot1Buffer.loadFromFile("assets/shoot1.wav"))
			std::cout << "[ERROR]: Could not load sound effect assets/shoot1.wav" << std::endl;

		if(!shoot2Buffer.loadFromFile("assets/shoot2.wav"))
			std::cout << "[ERROR]: Could not load sound effect assets/shoot2.wav" << std::endl;

		if(!powerdownBuffer.loadFromFile("assets/powerdown.wav"))
			std::cout << "[ERROR]: Could not load sound effect assets/powerdown.wav" << std::endl;

		if(!enemyDiedBuffer.loadFromFile("assets/enemydestroyed.wav"))
			std::cout << "[ERROR]: Could not load sound effect assets/enemydestroyed.wav" << std::endl;

		if(!mouseoverBuffer.loadFromFile("assets/mouseover.wav"))
			std::cout << "[ERROR]: Could not load sound effect assets/mouseover.wav" << std::endl;

		if(!hitEnemyBuffer.loadFromFile("assets/hitenemy.wav"))
			std::cout << "[ERROR]: Could not load sound effect assets/hitenemy.wav" << std::endl;

		if(!hitEnemyShieldBuffer.loadFromFile("assets/hitshield.wav"))
			std::cout << "[ERROR]: Could not load sound effect assets/hitshield.wav" << std::endl;


		player.Init("assets/player.png");
		objectsList.push_back(&player);

		homeplanet.Init();
		objectsList.push_back(&homeplanet);

		buybar.Init();

		moneyui.Init();
		objectsList.push_back(&moneyui);


		wavemanger.Init();

		miscui.Init();

		moneyui.money = 400;

		gameClock.restart();
		while(window.isOpen())
		{
			timeAcc += gameClock.restart();

			miscui.Update(timeAcc);
			buybar.Update(timeAcc);
			moneyui.Update(timeAcc);

			if(timeAcc >= timePerFrame)
			{
				timeAcc -= timePerFrame;


				sf::Event event;
				while(window.pollEvent(event))
				{
					if(event.type == sf::Event::Closed)
						window.close();
					if(event.type == sf::Event::Resized)
					{
						mainView.setSize(event.size.width, event.size.height);
					}
					if(event.type == sf::Event::KeyReleased)
					{
						if(event.key.code == sf::Keyboard::P && !miscui.isGameOver && miscui.timeSincePauseToggled.asSeconds() > 1)
						{
							miscui.timeSincePauseToggled = sf::Time::Zero;
							miscui.isPaused = !miscui.isPaused;
						}
						if(miscui.isGameOver && event.key.code == sf::Keyboard::Return)
							miscui.RestartGame();
					}
				}

				MousePos = window.mapPixelToCoords(sf::Mouse::getPosition(window));


				/* UPDATES */
				if(!miscui.isPaused && !miscui.isGameOver)
				{
					//Create exhaust trail
					if(rand() % 6 < 5 && (player.mvelocity.x != 0 || player.mvelocity.y != 0))
					{
						ParticleExhaust exh;
						exh.Init(player.GetExhaustPlacement(), player.GetRotation(), 1, 0);
						exhPartsList.push_back(exh);
					}


					for(unsigned int cnt = 0; cnt < objectsList.size(); cnt++)
					{
						objectsList[cnt]->Update(timePerFrame);
					}
					wavemanger.Update(timePerFrame);
					// Update all game objects
					for(unsigned int cnt = 0; cnt < exhPartsList.size(); cnt++)
					{
						exhPartsList[cnt].Update(timePerFrame);
					}
					for(unsigned int cnt = 0; cnt < missilesList.size(); cnt++)
					{
						missilesList[cnt].Update(timePerFrame);
					}
					for(unsigned int cnt = 0; cnt < explosionsList.size(); cnt++)
					{
						explosionsList[cnt].Update(timePerFrame);
					}
					for(unsigned int cnt = 0; cnt < bulletsList.size(); cnt++)
					{
						bulletsList[cnt].Update(timePerFrame);
					}

					// Check if any need to be deleted
					for(unsigned int cnt = 0; cnt < exhPartsList.size(); cnt++)
					{
						if(!exhPartsList[cnt].IsStillAlive())
						{
							exhPartsList.erase(exhPartsList.begin() + cnt);
							--cnt;
						}
					}
					for(unsigned int cnt = 0; cnt < missilesList.size(); cnt++)
					{
						if(!missilesList[cnt].IsStillAlive())
						{
							missilesList.erase(missilesList.begin() + cnt);
							--cnt;
						}
					}
					for(unsigned int cnt = 0; cnt < explosionsList.size(); cnt++)
					{
						if(!explosionsList[cnt].IsStillAlive())
						{
							explosionsList.erase(explosionsList.begin() + cnt);
							--cnt;
						}
					}
					for(unsigned int cnt = 0; cnt < bulletsList.size(); cnt++)
					{
						if(!bulletsList[cnt].IsStillAlive())
						{
							bulletsList.erase(bulletsList.begin() + cnt);
							--cnt;
						}
					}

					soundmanager.Update();
				}
				//} //Had to cap FPS to make it smooth ... :(

				/* DRAW GAME OBJECTS */
				{
					window.setView(mainView);
					mainView.setCenter(objectsList[0]->msprite.getPosition());
					window.clear(sf::Color(75, 0, 130, 255));

					homeplanet.Draw(window);
					wavemanger.Draw(window);

					for(unsigned int cnt = 0; cnt < exhPartsList.size(); cnt++)
					{
						exhPartsList[cnt].Draw(window);
					}
					for(unsigned int cnt = 0; cnt < bulletsList.size(); cnt++)
					{
						bulletsList[cnt].Draw(window);
					}
					for(unsigned int cnt = 0; cnt < missilesList.size(); cnt++)
					{
						missilesList[cnt].Draw(window);
					}
					for(unsigned int cnt = 0; cnt < explosionsList.size(); cnt++)
					{
						explosionsList[cnt].Draw(window);
					}
					player.Draw(window);
					buybar.Draw(window);
					moneyui.Draw(window);

					if(homeplanet.HasScanner())
						wavemanger.DrawAlerts(window);

					miscui.Draw(window);

					window.display();
				}
			}
		}
	}

	else
	{
		// LOAD MENU TEXTURES (LAMELY)
		if(!buttonDefaultTexture.loadFromFile("assets/buttondefault.png"))
			std::cout << "[ERROR]: Could not load texture assets/buttondefault.png" << std::endl;
		if(!buttonMouseOverTexture.loadFromFile("assets/buttonmouseover.png"))
			std::cout << "[ERROR]: Could not load texture assets/buttonmouseover.png" << std::endl;
		if(!textMainTexture.loadFromFile("assets/titlemain.png"))
			std::cout << "[ERROR]: Could not load texture assets/titlemain.png" << std::endl;
		if(!textSubTexture.loadFromFile("assets/titlesub.png"))
			std::cout << "[ERROR]: Could not load texture assets/titlesub.png" << std::endl;
		if(!menuExhaustTexture.loadFromFile("assets/shipexhaust.png"))
			std::cout << "[ERROR]: Could not load texture assets/shipexhaust.png" << std::endl;
		if(!menuShipTexture.loadFromFile("assets/shipsplash.png"))
			std::cout << "[ERROR]: Could not load texture assets/shipsplash.png" << std::endl;
		if(!planetSplashTexture.loadFromFile("assets/splashplanet.png"))
			std::cout << "[ERROR]: Could not load texture assets/splashplanet.png" << std::endl;

		if(!playButtonFont.loadFromFile("assets/Orbitron-Black.ttf"))
			std::cout << "[ERROR]: Could not load font assets/Orbitron-Black.ttf" << std::endl;

		if(!mouseoverBuffer.loadFromFile("assets/mouseover.wav"))
			std::cout << "[ERROR]: Could not load sound effect assets/mouseover.wav" << std::endl;


		playButtonText.setFont(playButtonFont);
		playButtonText.setCharacterSize(50);
		playButtonText.setString("P L A Y");
		playButtonText.setOrigin(playButtonText.getGlobalBounds().width / 2, playButtonText.getGlobalBounds().height / 2);

		sf::Sprite playButton;
		playButton.setTexture(buttonDefaultTexture);
		playButton.setOrigin(playButton.getGlobalBounds().width / 2, playButton.getGlobalBounds().height / 2);
		playButton.setScale(1.f, 0.45);

		sf::Sprite ship;
		ship.setTexture(menuShipTexture);
		ship.setOrigin(300.f, 200.f);

		sf::Sprite titleMain;
		titleMain.setTexture(textMainTexture);
		titleMain.setOrigin(titleMain.getGlobalBounds().width / 2, titleMain.getGlobalBounds().height / 2);

		sf::Sprite titleSub;
		titleSub.setTexture(textSubTexture);
		titleSub.setOrigin(titleSub.getGlobalBounds().width / 2, titleSub.getGlobalBounds().height / 2);

		sf::Sprite planet;
		planet.setTexture(planetSplashTexture);
		planet.setOrigin(planet.getGlobalBounds().width / 2, planet.getGlobalBounds().height / 2);

		mouseOverMenuSound.setBuffer(mouseoverBuffer);
		sf::Time timeSinceMouseOver = sf::Time::Zero;

		sf::Time timeSinceLastParticle = sf::Time::Zero;
		std::vector<MenuExhaustParticles> menuExhaustList;

		sf::Time timeInMenu = sf::Time::Zero;

		gameClock.restart();
		while(window.isOpen())
		{
			timeAcc += gameClock.restart();
			while(timeAcc >= timePerFrame)
			{
				timeAcc -= timePerFrame;

				timeSinceMouseOver += timePerFrame;
				timeSinceLastParticle += timePerFrame;
				timeInMenu += timePerFrame;

				sf::Event event;
				while(window.pollEvent(event))
				{
					if(event.type == sf::Event::Closed)
						window.close();
					if(event.type == sf::Event::Resized)
					{
						mainView.setSize(event.size.width, event.size.height);
					}
				}

				MousePos = window.mapPixelToCoords(sf::Mouse::getPosition(window));

				if(playButton.getGlobalBounds().contains(MousePos))
				{
					playButton.setTexture(buttonMouseOverTexture);
					if(timeSinceMouseOver.asSeconds() > 0.1)
					{
						mouseOverMenuSound.play();
					}
					timeSinceMouseOver = sf::Time::Zero;
					if(sf::Mouse::isButtonPressed(sf::Mouse::Left))
					{
						isOnStartScreen = false;
						main(0, NULL);
					}
				}
				else
				{
					playButton.setTexture(buttonDefaultTexture);
				}


				if(timeSinceLastParticle.asSeconds() > 0.15)
				{
					timeSinceLastParticle = sf::Time::Zero;
					MenuExhaustParticles particle;
					menuExhaustList.push_back(particle);
					menuExhaustList[menuExhaustList.size() - 1].Init(menuExhaustTexture,
					        sf::Vector2f(ship.getPosition().x + 650, ship.getPosition().y - 75), sf::seconds(1.5));
				}

				for(unsigned int cnt = 0; cnt < menuExhaustList.size(); cnt++)
				{
					menuExhaustList[cnt].Update(timePerFrame);
				}
				for(unsigned int cnt = 0; cnt < menuExhaustList.size(); cnt++)
				{
					if(!menuExhaustList[cnt].IsStillAlive())
					{
						menuExhaustList.erase(menuExhaustList.begin() + cnt);
						--cnt;
					}
				}

				titleMain.setPosition(mainView.getCenter().x, mainView.getCenter().y - mainView.getSize().y / 3 + 5 * sin(timeInMenu.asSeconds()));
				titleSub.setPosition(titleMain.getPosition().x, titleMain.getPosition().y + 75);

				playButton.setPosition(mainView.getCenter().x - mainView.getSize().x / 3.5, mainView.getCenter().y + mainView.getSize().y / 5);
				playButtonText.setPosition(playButton.getPosition().x, playButton.getPosition().y - 15);
				ship.setPosition(mainView.getCenter().x + 25 * cos(timeInMenu.asSeconds()),
				                 mainView.getCenter().y  + 25 * cos(timeInMenu.asSeconds()));

				planet.setPosition(mainView.getCenter().x - mainView.getSize().x / 2.5, mainView.getCenter().y - mainView.getSize().y / 2.5);
				planet.setScale(mainView.getSize().y / 600, mainView.getSize().y / 600);
				planet.rotate(0.075);

				window.setView(mainView);
				window.clear(sf::Color(75, 0, 130, 255));

				window.draw(planet);
				for(unsigned int cnt = 0; cnt < menuExhaustList.size(); cnt++)
				{
					menuExhaustList[cnt].Draw(window);
				}
				window.draw(ship);
				window.draw(titleMain);
				window.draw(titleSub);
				window.draw(playButton);
				window.draw(playButtonText);
				window.display();
			}
		}
	}



	return 0;
}
