#ifndef GAMEOBJECT_H
#define GAMEOBJECT_H

#include <SFML/Graphics.hpp>

class GameObject
{
public:
	sf::Sprite msprite;
	
	enum ObjectType {
		PLAYER, PLANET
	};
	
	ObjectType mtype;
	
	GameObject();
	~GameObject();
	
	sf::Vector2f GetPosition();
	float GetRotation();
	sf::Transform GetTransform();
	sf::FloatRect GetGlobalBounds();
	float DistanceFrom(sf::Vector2f _other);
	
	virtual void Init();
	virtual void Update(sf::Time _deltaTime);
	virtual void Draw(sf::RenderWindow& _window);
	virtual bool IsStillAlive();
};

#endif // GAMEOBJECT_H
