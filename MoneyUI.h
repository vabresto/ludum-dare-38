#ifndef MONEYUI_H
#define MONEYUI_H

#include <SFML/Graphics.hpp>
#include <vector>

#include "GameObject.h"

class MoneyUI : public GameObject
{
	sf::Texture moneyUITexture;
	
	sf::Sprite enemiesCounterSprite;
	
	sf::Text enemiesLeftText;
	
	sf::Text waveText;
	sf::Text moneyText;
	
	
	sf::Sprite energyBarCover;
	sf::Sprite energyBar;
	sf::Text energyText;
	sf::Text numMissilesArmedText;
	
	char energyTextString[1024];
	char numMissilesArmedString[1024];
	
	bool energyPositive;
	
public:
	MoneyUI();
	~MoneyUI();
	
	int waveNumber, money;
	
	void Init();
	void Update(sf::Time _deltaTime);
	void Draw(sf::RenderWindow& _window);
	
	void ResetMoneyUI();
};

#endif // MONEYUI_H
