#include <random>
#include "SoundManager.h"
#include "Globals.h"

SoundManager::SoundManager()
{
}

SoundManager::~SoundManager()
{
}

void SoundManager::Update()
{
	for(unsigned int cnt = 0; cnt < soundsList.size(); cnt++)
	{
		if(soundsList[cnt].getStatus() == sf::Sound::Stopped)
		{
			soundsList.erase(soundsList.begin() + cnt);
			--cnt;
		}
	}
}

void SoundManager::CreateSound(SoundEffect _request)
{
	if(soundsList.size() < 200)
	{
		sf::Sound sound;
		switch(_request)
		{
		case GAMEOVER:
		{
			sound.setBuffer(gameoverBuffer);
			break;
		}
		case LEVELCOMPLETE:
		{
			if(rand() % 2 == 0)
				sound.setBuffer(levelCompleteBuffer);
			else
				sound.setBuffer(levelComplete2Buffer);
			break;
		}
		case STARTBUILDING:
		{
			sound.setBuffer(startBuildingBuffer);
			break;
		}
		case DONEBUILDING:
		{
			sound.setBuffer(doneBuildingBuffer);
			break;
		}
		case PLANETHIT:
		{
			sound.setBuffer(planetHitBuffer);
			break;
		}
		case PLANETRECOVERED:
		{
			sound.setBuffer(planetRecoveredBuffer);
			break;
		}
		case ALLIEDSHIELDREGEN:
		{
			sound.setBuffer(alliedShieldRegenBuffer);
			break;
		}
		case ENEMYSHIELDREGEN:
		{
			sound.setBuffer(enemyShieldRegenBuffer);
			break;
		}
		case MISSILEEXPLODE:
		{
			sound.setBuffer(missileExplodeBuffer);
			break;
		}
		case SHOOT:
		{
			if(rand() % 2 == 0)
				sound.setBuffer(shoot1Buffer);
			else
				sound.setBuffer(shoot2Buffer);
			break;
		}
		case POWERDOWN:
		{
			sound.setBuffer(powerdownBuffer);
			break;
		}
		case ENEMYDIED:
		{
			sound.setBuffer(enemyDiedBuffer);
			break;
		}
		case ENEMYSHIELDHIT:
		{
			sound.setBuffer(hitEnemyShieldBuffer);
			break;
		}
		case ENEMYHIT:
		{
			sound.setBuffer(hitEnemyBuffer);
			break;
		}
		case MOUSEOVER:
		{
			sound.setBuffer(mouseoverBuffer);
			break;
		}
		}

		soundsList.push_back(sound);
		soundsList[soundsList.size() - 1].play();
	}
}
