#ifndef PARTICLEEXHAUST_H
#define PARTICLEEXHAUST_H

#include "GameObject.h"

class ParticleExhaust : public GameObject
{
	sf::Time timeToLive;
public:
	ParticleExhaust();
	~ParticleExhaust();

public:
	void Init(sf::Vector2f _pos, float _rot, float _scale, int _team);
	bool IsStillAlive();
	void Update(sf::Time _deltaTime);
};

#endif // PARTICLEEXHAUST_H
