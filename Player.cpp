#include <SFML/Graphics.hpp>
#include <iostream>
#include <math.h>

#include "Player.h"
#include "Bullet.h"
#include "Globals.h"

Player::Player()
{
}

Player::~Player()
{
}

void Player::Init(std::string _textFileName)
{
	if(!mtexture.loadFromFile(_textFileName))
		std::cout << "[ERROR]: Could not load texture " << _textFileName << std::endl;

	msprite.setTexture(mtexture);

	msprite.setOrigin(32.f, 32.f);
	msprite.setPosition(250.f, 250.f);
	msprite.setRotation(90.f);

	mMaxSpeed = 10;
	mDeltaSpeed = 2;
	
	timeSinceLastFired = sf::Time::Zero;
	fireCooldown = sf::seconds(0.15);

	mtype = GameObject::PLAYER;
}

void Player::Update(sf::Time _deltaTime)
{
	timeSinceLastFired += _deltaTime;
	bool isKeyPressed = false;
	if(sf::Keyboard::isKeyPressed(sf::Keyboard::W))
	{
		isKeyPressed = true;
		if(mvelocity.y > -1 * mMaxSpeed)
			mvelocity.y -= mDeltaSpeed;
		else
			mvelocity.y = -1 * mMaxSpeed;
	}
	else if(sf::Keyboard::isKeyPressed(sf::Keyboard::S))
	{
		isKeyPressed = true;
		if(mvelocity.y < mMaxSpeed)
			mvelocity.y += mDeltaSpeed;
		else
			mvelocity.y = mMaxSpeed;
	}
	else
	{
		mvelocity.y = 0;
	}

	if(sf::Keyboard::isKeyPressed(sf::Keyboard::A))
	{
		isKeyPressed = true;
		if(mvelocity.x > -1 * mMaxSpeed)
			mvelocity.x -= mDeltaSpeed;
		else
			mvelocity.x = -1 * mMaxSpeed;
	}
	else if(sf::Keyboard::isKeyPressed(sf::Keyboard::D))
	{
		isKeyPressed = true;
		if(mvelocity.x < mMaxSpeed)
			mvelocity.x += mDeltaSpeed;
		else
			mvelocity.x = mMaxSpeed;
	}
	else
	{
		mvelocity.x = 0;
	}

	if(isKeyPressed)
		msprite.setRotation(atan2(mvelocity.y, mvelocity.x) * 180 / 3.14159 + 90);


	msprite.move(mvelocity);
	
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::Space) && timeSinceLastFired >= fireCooldown)
	{
		timeSinceLastFired = sf::Time::Zero;
		soundmanager.CreateSound(SoundManager::SHOOT);
		
		Bullet bullet;
		bullet.Init(msprite.getPosition(), RotationToDirection(msprite.getRotation()), 0.025, sf::seconds(10));
		bulletsList.push_back(bullet);
	}
}
sf::Vector2f Player::GetExhaustPlacement()
{
	sf::Vector2f ret = msprite.getPosition();
	ret.x -= 0.65 * mvelocity.x;
	ret.y -= 0.65 * mvelocity.y;
	return ret;
}

void Player::ResetPlayer()
{
	msprite.setPosition(250.f, 250.f);
	timeSinceLastFired = sf::Time::Zero;
	fireCooldown = sf::seconds(0.15);
}
