#ifndef ICON_H
#define ICON_H

#include <SFML/Graphics.hpp>
#include "GameObject.h"
#include "Building.h"

class Icon : public GameObject
{
	sf::IntRect textureBounds;
	bool isMouseOver;
	sf::Vector2f offset;
	Building::StructureType structuretype;
	
	
public:
	Icon();
	~Icon();
	
	float cost;

	void Init(sf::Texture& _texture, sf::IntRect _rect, sf::Vector2f _offset, Building::StructureType _structuretype, float _cost);
	void Update(sf::Time _deltaTime);
	void Draw(sf::RenderWindow& _window);
	
	bool CanBuy();
};

#endif // ICON_H
