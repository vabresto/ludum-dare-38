#ifndef EXPLOSION_H
#define EXPLOSION_H

#include "GameObject.h"

class Explosion : public GameObject
{
	sf::Time timeToLive;
	int stage;
public:
	Explosion();
	~Explosion();

	void Init(sf::Vector2f _center);
	void Update(sf::Time _deltaTime);
	bool IsStillAlive();
};

#endif // EXPLOSION_H
