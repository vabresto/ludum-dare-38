#ifndef ENEMY_H
#define ENEMY_H

#include "GameObject.h"

class Enemy : public GameObject
{
public:
	Enemy();
	~Enemy();
	
	float length;
	float health;
	float moveSpeed;
	
	sf::Vector2f moveTarget;
	float moneyValue;
	sf::Time timeSinceHitByExplosion;
	
	bool isDamaged;
	sf::Time damagedTimer;
	

	void Init();
	void Update(sf::Time _deltaTime);
	void Draw(sf::RenderWindow& _window);
	bool IsStillAlive();
	
	float GetValue();
};

#endif // ENEMY_H
