#include "BuyBarIndicator.h"
#include "Globals.h"

BuyBarIndicator::BuyBarIndicator()
{
}

BuyBarIndicator::~BuyBarIndicator()
{
}

void BuyBarIndicator::Init(sf::Vector2f _pos, int _cost)
{
	msprite.setTexture(exhaustEnemyTexture);
	msprite.setScale(5.f, 4.f);

	cost = _cost;
	canAfford = false;
}
void BuyBarIndicator::Update(sf::Time _deltaTime)
{
	if(moneyui.money >= cost && !canAfford)
	{
		msprite.setTexture(exhaustGoodTexture);
		canAfford = true;
	}
	else if(moneyui.money < cost && canAfford)
	{
		msprite.setTexture(exhaustEnemyTexture);
		canAfford = false;
	}
}

void BuyBarIndicator::UpdatePosition(sf::Vector2f _newPos)
{
	msprite.setPosition(_newPos.x - 5.f, _newPos.y - 35.f);
}
