#include "MiscUI.h"
#include "Globals.h"

#include <iostream>

MiscUI::MiscUI()
{
}

MiscUI::~MiscUI()
{
}

void MiscUI::Init()
{
	gameOverSound.setBuffer(gameoverBuffer);
	
	isPaused = false;
	isGameOver = false;
	PlayedGameOverSound = false;
	timeSincePauseToggled = sf::Time::Zero;

	pausedText.setFont(mainFont);
	pausedText.setCharacterSize(60);
	pausedText.setString("P A U S E D");
	pausedText.setOrigin(pausedText.getGlobalBounds().width / 2, 0.f);

	gameOverText.setFont(mainFont);
	gameOverText.setCharacterSize(75);
	gameOverText.setColor(sf::Color::Red);
	gameOverText.setString("GAME OVER");
	gameOverText.setOrigin(gameOverText.getGlobalBounds().width / 2, 0.f);

	gameOverText2.setFont(subFont);
	gameOverText2.setCharacterSize(50);
	gameOverText2.setColor(sf::Color::Magenta);
	gameOverText2.setString("[ENTER]  TO  RESTART");
	gameOverText2.setOrigin(gameOverText2.getGlobalBounds().width / 2, 0.f);


	pauseOverlay.setTexture(pauseOverlayTexture);
	pauseOverlay.setOrigin(64.f, 64.f);
	pauseOverlay.setColor(sf::Color(255, 255, 255, 126));
}

void MiscUI::Update(sf::Time _deltaTime)
{
	timeSincePauseToggled += _deltaTime;

	pausedText.setPosition(mainView.getCenter().x, mainView.getCenter().y - mainView.getSize().y / 3);
	gameOverText.setPosition(mainView.getCenter().x, mainView.getCenter().y - mainView.getSize().y / 3);
	gameOverText2.setPosition(mainView.getCenter().x, mainView.getCenter().y + mainView.getSize().y / 5);

	pauseOverlay.setPosition(mainView.getCenter());
	pauseOverlay.setScale(mainView.getSize().x / 128, mainView.getSize().y / 128);
}

void MiscUI::Draw(sf::RenderWindow& _window)
{
	if(isGameOver)
	{
		_window.draw(pauseOverlay);
		_window.draw(gameOverText);
		_window.draw(gameOverText2);
		
		if (!PlayedGameOverSound)
			gameOverSound.play();
	}
	else if(isPaused)
	{
		_window.draw(pauseOverlay);
		_window.draw(pausedText);
	}
}

void MiscUI::RestartGame()
{
	isGameOver = false;
	PlayedGameOverSound = false;
	homeplanet.ResetPlanet();
	player.ResetPlayer();
	moneyui.ResetMoneyUI();

	exhPartsList.clear();
	bulletsList.clear();
	missilesList.clear();
}
