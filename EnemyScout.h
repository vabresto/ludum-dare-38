#ifndef ENEMYSCOUT_H
#define ENEMYSCOUT_H

#include "Enemy.h"

class EnemyScout : public Enemy
{
public:
	EnemyScout();
	~EnemyScout();

	void Init(sf::Vector2f _spawn);
	void Update(sf::Time _deltaTime);
};

#endif // ENEMYSCOUT_H
