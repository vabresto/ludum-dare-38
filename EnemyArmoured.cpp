#include "EnemyArmoured.h"
#include "Globals.h"

EnemyArmoured::EnemyArmoured()
{
}

EnemyArmoured::~EnemyArmoured()
{
}

void EnemyArmoured::Init(sf::Vector2f _spawn)
{
	msprite.setTexture(armouredEnemyTexture);
	msprite.setOrigin(32.f,32.f);
	
	msprite.setPosition(_spawn);
	
	moveTarget = homeplanet.GetPosition();
	moveSpeed = 5.f;
	health = 3;
	
	length = 12;
	
	moneyValue = 100;
}
