#ifndef BUYBARINDICATOR_H
#define BUYBARINDICATOR_H

#include "GameObject.h"

class BuyBarIndicator : public GameObject
{
public:
	BuyBarIndicator();
	~BuyBarIndicator();

	int cost;
	bool canAfford;

public:

	void Init(sf::Vector2f _pos, int _cost);

	void Update(sf::Time _deltaTime);
	
	void UpdatePosition(sf::Vector2f _newPos);
};

#endif // BUYBARINDICATOR_H
