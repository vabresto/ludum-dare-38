#ifndef BUYBAR_H
#define BUYBAR_H

#include <SFML/Graphics.hpp>
#include <vector>

#include "GameObject.h"
#include "Icon.h"
#include "BuyBarIndicator.h"

class BuyBar : public GameObject
{
	sf::Texture buyBarTopTexture;
	sf::Texture buyBarBottomTexture;
	
	sf::Sprite buyBarTop;
	std::vector<Icon> buyBarIcons;
	std::vector<sf::Text> buyBarCosts;
	std::vector<BuyBarIndicator> indicatorsList;
	
public:
	BuyBar();
	~BuyBar();
	
	unsigned int cannonCost, solarPanelCost, missileLauncherCost, shieldGenCost, batteryCost;
	

	void Init();
	void Update(sf::Time _deltaTime);
	void Draw(sf::RenderWindow& _window);
};

#endif // BUYBAR_H
