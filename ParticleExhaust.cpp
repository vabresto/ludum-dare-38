#include <SFML/Graphics.hpp>
#include <iostream>
#include <random>
#include "ParticleExhaust.h"

#include "Globals.h"

const float lifespan = 0.5;

ParticleExhaust::ParticleExhaust()
{
}

ParticleExhaust::~ParticleExhaust()
{
}

void ParticleExhaust::Init(sf::Vector2f _pos, float _rot, float _scale, int _team)
{
	if (_team == 0)
		msprite.setTexture(exhaustGoodTexture);
	else
		msprite.setTexture(exhaustEnemyTexture);
		
	
	msprite.setPosition(_pos);
	msprite.setScale(_scale, _scale);
	msprite.setOrigin(8.f, 4.f);
	
	msprite.setRotation(_rot);
	
	timeToLive = sf::seconds(lifespan);
	//std::cout << "Spawned exhaust!" << std::endl;
}
bool ParticleExhaust::IsStillAlive()
{
	return (timeToLive.asSeconds() > 0);
}
void ParticleExhaust::Update(sf::Time _deltaTime)
{
	timeToLive -= _deltaTime;
	msprite.setColor(sf::Color(255,255,255, 255 * timeToLive.asSeconds() / lifespan));
}
