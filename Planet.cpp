#include <SFML/Graphics.hpp>
#include <iostream>
#include <random>
#include "Planet.h"
#include "Globals.h"

Planet::Planet()
{
}

Planet::~Planet()
{
}

void Planet::Init()
{
	//TODO: UNTOGGLE THESE
	DEBUG_IS_INVULNERABLE = false;
	
	
	
	hasScanner = true;
	hasShields= false;
	poweredDown = false;
	
	
	msprite.setTexture(planetsTexture);
	health = 3;
	msprite.setTextureRect(sf::IntRect(0, 0, 128, 128));
	msprite.setScale(2.f, 2.f);
	msprite.setOrigin(64.f, 64.f);
	msprite.setPosition(250.f, 250.f);
	
	hitbox.setRadius(120);
	hitbox.setOrigin(120.f, 120.f);
	hitbox.setOutlineColor(sf::Color::Red);
	hitbox.setFillColor(sf::Color::Transparent);
	hitbox.setOutlineThickness(2);
	hitbox.setPosition(msprite.getPosition());

	baseEnergy = 500;
	baseEnergyCap = 20000;
	baseRegenRate = 100;

	buildProgress = 0;
	isBuilding = false;

	progressBar.setTexture(progressBarTexture);
	progressBar.setPosition(msprite.getPosition().x - 128.f, msprite.getPosition().y - 160.f);
	progressBar.setScale(2.f,1.f);

	progressBarCover.setTexture(progressBarCoverTexture);
	progressBarCover.setPosition(msprite.getPosition().x + 128.f, msprite.getPosition().y - 160.f);
	progressBarCover.setOrigin(128.f,0.f);
	progressBarCover.setScale(2.f,1.f);
	
	
	
	
	currentEnergy = baseEnergy;
	energyCap = baseEnergyCap;
	energyRegenRate = baseRegenRate;
}

void Planet::Update(sf::Time _deltaTime)
{
	msprite.rotate(0.1);
	Building::timeSinceLastMissileFired += _deltaTime;
	
	if (currentEnergy <= energyCap - energyRegenRate * _deltaTime.asSeconds())
	{
		currentEnergy += energyRegenRate * _deltaTime.asSeconds();
	}
	else
		currentEnergy = energyCap;
		
	if (currentEnergy < 0)
		currentEnergy = 0;

	if(isBuilding)
	{
		buildProgress += _deltaTime.asSeconds();

		msprite.setColor(sf::Color(0, 126, 126, 255));

		switch(beingBuilt)
		{
		case Building::CANNON:
		{
			progressBarCover.setScale(2 * (CannonBuildTime - buildProgress)/CannonBuildTime, 1.f);
			
			if(buildProgress >= CannonBuildTime)
			{
				isBuilding = false;
				buildProgress = 0;
				std::cout << "Cannon completed!" << std::endl;
				msprite.setColor(sf::Color::White);

				Building newbuild;
				newbuild.Init(Building::CANNON);
				buildings.push_back(newbuild);
				
				soundmanager.CreateSound(SoundManager::DONEBUILDING);
			}
			break;
		}
		case Building::SOLARPANEL:
		{
			progressBarCover.setScale(2 * (ScannerBuildTime - buildProgress)/ScannerBuildTime, 1.f);
			if(buildProgress >= ScannerBuildTime)
			{
				isBuilding = false;
				buildProgress = 0;
				std::cout << "Solar panel completed!" << std::endl;
				msprite.setColor(sf::Color::White);
				Building newbuild;
				newbuild.Init(Building::SOLARPANEL);
				buildings.push_back(newbuild);
				
				soundmanager.CreateSound(SoundManager::DONEBUILDING);
			}
			break;
		}
		case Building::SHIELDGENERATOR:
		{
			progressBarCover.setScale(2 * (ScannerBuildTime - buildProgress)/ScannerBuildTime, 1.f);
			if(buildProgress >= ScannerBuildTime)
			{
				isBuilding = false;
				buildProgress = 0;
				hasShields =true;
				std::cout << "Shield generator completed!" << std::endl;
				msprite.setColor(sf::Color::White);
				Building newbuild;
				newbuild.Init(Building::SHIELDGENERATOR);
				buildings.push_back(newbuild);
				
				soundmanager.CreateSound(SoundManager::DONEBUILDING);
			}
			break;
		}
		case Building::BATTERY:
		{
			progressBarCover.setScale(2 * (ScannerBuildTime - buildProgress)/ScannerBuildTime, 1.f);
			if(buildProgress >= ScannerBuildTime)
			{
				isBuilding = false;
				buildProgress = 0;
				std::cout << "Battery completed!" << std::endl;
				msprite.setColor(sf::Color::White);
				Building newbuild;
				newbuild.Init(Building::BATTERY);
				buildings.push_back(newbuild);
				
				soundmanager.CreateSound(SoundManager::DONEBUILDING);
			}
			break;
		}
		case Building::MISSILELAUNCHER:
		{
			progressBarCover.setScale(2 * (MissileLauncherBuildTime - buildProgress)/MissileLauncherBuildTime, 1.f);
			if(buildProgress >= MissileLauncherBuildTime)
			{
				isBuilding = false;
				buildProgress = 0;
				std::cout << "Missile launcher completed!" << std::endl;
				msprite.setColor(sf::Color::White);
				Building newbuild;
				newbuild.Init(Building::MISSILELAUNCHER);
				buildings.push_back(newbuild);
				
				soundmanager.CreateSound(SoundManager::DONEBUILDING);
			}
			break;
		}
		}
	}

	for(unsigned int cnt = 0; cnt < buildings.size(); cnt++)
	{
		buildings[cnt].Update(_deltaTime);
	}
	
	
	if (hasShields && energyRegenRate < 0 && currentEnergy < 500)
	{
		soundmanager.CreateSound(SoundManager::POWERDOWN);
		poweredDown = true;
	}
	else if (hasShields && currentEnergy > 500 && poweredDown)
	{
		poweredDown = false;
		soundmanager.CreateSound(SoundManager::ALLIEDSHIELDREGEN);
	}
}

void Planet::ConstructBuilding(Building::StructureType _construct)
{
	if(!isBuilding)
	{
		std::cout << "Starting construction!" << std::endl;
		soundmanager.CreateSound(SoundManager::STARTBUILDING);
		isBuilding = true;
		beingBuilt = _construct;
		progressBarCover.setScale(2.f, 1.f);
		
		switch(_construct)
		{
			case Building::CANNON:
			{
				moneyui.money -= buybar.cannonCost;
				break;
			}
			case Building::SOLARPANEL:
			{
				moneyui.money -= buybar.solarPanelCost;
				break;
			}
			case Building::MISSILELAUNCHER:
			{
				moneyui.money -= buybar.missileLauncherCost;
				break;
			}
			case Building::SHIELDGENERATOR:
			{
				moneyui.money -= buybar.shieldGenCost;
				break;
			}
			case Building::BATTERY:
			{
				moneyui.money -= buybar.batteryCost;
				break;
			}
		}
	}
}

void Planet::Draw(sf::RenderWindow& _window)
{
	_window.draw(msprite);

	for(unsigned int cnt = 0; cnt < buildings.size(); cnt++)
	{
		buildings[cnt].Draw(_window);
	}

	if(isBuilding)
	{
		_window.draw(progressBar);
		_window.draw(progressBarCover);
	}
	
	//_window.draw(hitbox);

}

sf::CircleShape& Planet::GetHitbox()
{
	return hitbox;
}

void Planet::TakeDamage(int _dmg)
{
	if (_dmg > 0)
		soundmanager.CreateSound(SoundManager::PLANETHIT);
	
	if (!DEBUG_IS_INVULNERABLE)
		health -= _dmg;
	
	if (health > 3)
		health = 3;
	
	switch(health)
	{
		case 0:
		{
			miscui.isGameOver = true;
			break;
		}
		case 1:
		{
			msprite.setTextureRect(sf::IntRect(0, 256, 128, 128));
			break;
		}
		case 2:
		{
			msprite.setTextureRect(sf::IntRect(0, 128, 128, 128));
			break;
		}
		case 3:
		{
			msprite.setTextureRect(sf::IntRect(0, 0, 128, 128));
			break;
		}
		default:
		{
			msprite.setTextureRect(sf::IntRect(0, 0, 128, 128));
			break;
		}
	}
}

void Planet::ResetPlanet()
{
	health = 3;
	msprite.setTextureRect(sf::IntRect(0, 0, 128, 128));
	buildings.clear();
	isBuilding = false;
	hasScanner = true;
	hasShields = false;
	poweredDown = false;
	
	currentEnergy = baseEnergy;
	energyCap = baseEnergyCap;
	energyRegenRate = baseRegenRate;
}

bool Planet::HasScanner()
{
	return hasScanner;
}

float Planet::GetEnergy()
{
	return currentEnergy;
}

float Planet::GetEnergyCap()
{
	return energyCap;
}

float Planet::GetEnergyRegenRate()
{
	return energyRegenRate;
}

int Planet::ArmedMissiles()
{
	int ret = 0;
	for (unsigned int cnt = 0; cnt < buildings.size(); cnt++)
	{
		if (buildings[cnt].GetIsArmed())
			ret++;
	}
	return ret;
}

bool Planet::isInFriendlyShieldBubble(sf::Vector2f _start)
{
	for (unsigned int cnt = 0; cnt < buildings.size(); cnt++)
	{
		if (buildings[cnt].IsInBubbleShield(_start))
			return true;
	}
	return false;
}

void Planet::ShieldHit(sf::Vector2f _start, float _damage)
{
	for (unsigned int cnt = 0; cnt < buildings.size(); cnt++)
	{
		if (buildings[cnt].IsInBubbleShield(_start))
		{
			currentEnergy -= _damage * 5;
			return;
		}
	}
}
