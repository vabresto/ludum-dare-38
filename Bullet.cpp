#include "Bullet.h"
#include "Globals.h"

Bullet::Bullet()
{
}

Bullet::~Bullet()
{
}

void Bullet::Init(sf::Vector2f _spawn, sf::Vector2f _dir, float _ms, sf::Time _life)
{	
	direction = _dir;
	moveSpeed = 0.5;
	
	//HARDCODED NOW
	timeToLive = sf::seconds(3);
	
	msprite.setTexture(bulletsTexture);
	msprite.setPosition(_spawn);
	msprite.setOrigin(4.f, 8.f);
	msprite.setRotation(atan2(direction.y, direction.x) * 180 / 3.14159 + 90);
}

void Bullet::Update(sf::Time _deltaTime)
{
	msprite.move(direction.x * moveSpeed, direction.y * moveSpeed);
	timeToLive -= _deltaTime;
}

bool Bullet::IsStillAlive()
{
	return timeToLive.asSeconds() > 0;
}

void Bullet::HitSomething()
{
	timeToLive = sf::Time::Zero;
}
