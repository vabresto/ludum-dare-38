#ifndef BUILDING_H
#define BUILDING_H

#include <SFML/Graphics.hpp>
#include "GameObject.h"
#include "Missile.h"

class Building : public GameObject
{
public:
	enum StructureType
	{
	   CANNON, SOLARPANEL, MISSILELAUNCHER, SHIELDGENERATOR, BATTERY
	};
private:
	StructureType structtype;
	sf::Vector2f offset;
	float theta;
	float radius;
	sf::Time fireCooldown;
	sf::Time timeSinceLastFired;

	bool isArmed;
	Missile mmissile;
	bool isShieldActive;
	
	float shieldBubbleHealth;
	sf::Sprite shieldBubble;
	sf::CircleShape hitbox;
	
	int missileLauncherId;
	
public:
	Building();
	~Building();
	
	static int numMissileLaunchers;
	static int currentMissileLauncher;
	static sf::Time timeSinceLastMissileFired;
	
	float energyUsage;
	float energyCapModifier;

	void Init(StructureType _type);
	void Update(sf::Time _deltaTime);
	void Draw(sf::RenderWindow& _window);
	
	bool GetIsArmed();
	
	bool IsInBubbleShield(sf::Vector2f _start);
};

#endif // BUILDING_H
