#include "GameObject.h"

GameObject::GameObject()
{
}

GameObject::~GameObject()
{
}

void GameObject::Init()
{
}

void GameObject::Update(sf::Time _deltaTime)
{
}

void GameObject::Draw(sf::RenderWindow& _window)
{
	_window.draw(msprite);
}
bool GameObject::IsStillAlive()
{
	return true;
}
sf::Vector2f GameObject::GetPosition()
{
	return msprite.getPosition();
}
float GameObject::GetRotation()
{
	return msprite.getRotation();
}

sf::Transform GameObject::GetTransform()
{
	return msprite.getTransform();
}

float GameObject::DistanceFrom(sf::Vector2f _other)
{
	return sqrt(pow(_other.x - msprite.getPosition().x, 2) + pow(_other.y - msprite.getPosition().y, 2));
}

sf::FloatRect GameObject::GetGlobalBounds()
{
	return msprite.getGlobalBounds();
}
