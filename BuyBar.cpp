#include <SFML/Graphics.hpp>
#include <iostream>
#include <vector>
#include <string>

#include "BuyBar.h"
#include "Globals.h"
#include "Building.h"
#include "BuyBarIndicator.h"

BuyBar::BuyBar()
{
}

BuyBar::~BuyBar()
{
}

void BuyBar::Init()
{
	if(!buyBarBottomTexture.loadFromFile("assets/buybarbottom.png"))
		std::cout << "[ERROR]: Could not load assets/buybarbottom.png" << std::endl;
	if(!buyBarTopTexture.loadFromFile("assets/buybartop.png"))
		std::cout << "[ERROR]: Could not load assets/buybartop.png" << std::endl;

	msprite.setTexture(buyBarBottomTexture);
	msprite.setOrigin(304.f, 128.f);
	buyBarTop.setTexture(buyBarTopTexture);
	buyBarTop.setOrigin(304.f, 128.f);

	cannonCost = 200;
	solarPanelCost = 250;
	missileLauncherCost = 500;
	shieldGenCost = 625;
	batteryCost = 225;


	Icon cannonIcon;
	cannonIcon.Init(buildingsTexture, sf::IntRect(32, 0, 32, 32), sf::Vector2f(-224.f, -72.f), Building::CANNON, cannonCost);
	buyBarIcons.push_back(cannonIcon);
	sf::Text cannonText;
	cannonText.setFont(subFont);
	cannonText.setCharacterSize(14);
	cannonText.setString("$" + std::to_string(cannonCost));
	buyBarCosts.push_back(cannonText);
	BuyBarIndicator cannonInd;
	cannonInd.Init(cannonIcon.GetPosition(), cannonCost);
	indicatorsList.push_back(cannonInd);


	Icon solarpanelIcon;
	solarpanelIcon.Init(buildingsTexture, sf::IntRect(32, 32, 32, 32), sf::Vector2f(-128.f, -72.f), Building::SOLARPANEL, solarPanelCost);
	buyBarIcons.push_back(solarpanelIcon);
	sf::Text solarpanelText;
	solarpanelText.setFont(subFont);
	solarpanelText.setCharacterSize(14);
	solarpanelText.setString("$" + std::to_string(solarPanelCost));
	buyBarCosts.push_back(solarpanelText);
	BuyBarIndicator solarpanelInd;
	solarpanelInd.Init(solarpanelIcon.GetPosition(), solarPanelCost);
	indicatorsList.push_back(solarpanelInd);


	Icon missileLauncherIcon;
	missileLauncherIcon.Init(buildingsTexture, sf::IntRect(32, 64, 32, 32), sf::Vector2f(-32.f, -72.f), Building::MISSILELAUNCHER, missileLauncherCost);
	buyBarIcons.push_back(missileLauncherIcon);
	sf::Text missileLauncherText;
	missileLauncherText.setFont(subFont);
	missileLauncherText.setCharacterSize(14);
	missileLauncherText.setString("$" + std::to_string(missileLauncherCost));
	buyBarCosts.push_back(missileLauncherText);
	BuyBarIndicator missileLauncherInd;
	missileLauncherInd.Init(missileLauncherIcon.GetPosition(), missileLauncherCost);
	indicatorsList.push_back(missileLauncherInd);
	
	
	Icon shieldGenIcon;
	shieldGenIcon.Init(buildingsTexture, sf::IntRect(32, 96, 32, 32), sf::Vector2f(64.f, -72.f), Building::SHIELDGENERATOR, shieldGenCost);
	buyBarIcons.push_back(shieldGenIcon);
	sf::Text shieldGenText;
	shieldGenText.setFont(subFont);
	shieldGenText.setCharacterSize(14);
	shieldGenText.setString("$" + std::to_string(shieldGenCost));
	buyBarCosts.push_back(shieldGenText);
	BuyBarIndicator shieldGenInd;
	shieldGenInd.Init(shieldGenIcon.GetPosition(), shieldGenCost);
	indicatorsList.push_back(shieldGenInd);
	
	
	Icon batteryIcon;
	batteryIcon.Init(buildingsTexture, sf::IntRect(32, 128, 32, 32), sf::Vector2f(160, -72.f), Building::BATTERY, batteryCost);
	buyBarIcons.push_back(batteryIcon);
	sf::Text batteryText;
	batteryText.setFont(subFont);
	batteryText.setCharacterSize(14);
	batteryText.setString("$" + std::to_string(batteryCost));
	buyBarCosts.push_back(batteryText);
	BuyBarIndicator batteryInd;
	batteryInd.Init(batteryIcon.GetPosition(), batteryCost);
	indicatorsList.push_back(batteryInd);
}

void BuyBar::Update(sf::Time _deltaTime)
{
	msprite.setPosition(mainView.getCenter().x, mainView.getCenter().y + mainView.getSize().y / 2);
	buyBarTop.setPosition(msprite.getPosition());

	for(unsigned int cnt = 0; cnt < buyBarIcons.size(); cnt++)
	{
		buyBarIcons[cnt].Update(_deltaTime);
		indicatorsList[cnt].Update(_deltaTime);
	}

	for(unsigned int cnt = 0; cnt < buyBarCosts.size(); cnt++)
	{
		buyBarCosts[cnt].setPosition(buyBarIcons[cnt].GetPosition().x + 4, buyBarIcons[cnt].GetPosition().y - 30.f);
		indicatorsList[cnt].UpdatePosition(buyBarIcons[cnt].GetPosition());
	}
}

void BuyBar::Draw(sf::RenderWindow& _window)
{
	for(unsigned int cnt = 0; cnt < buyBarIcons.size(); cnt++)
	{
		buyBarIcons[cnt].Draw(_window);
		indicatorsList[cnt].Draw(_window);
	}

	_window.draw(msprite);

	for(unsigned int cnt = 0; cnt < buyBarCosts.size(); cnt++)
	{
		_window.draw(buyBarCosts[cnt]);
	}

	_window.draw(buyBarTop);
}
