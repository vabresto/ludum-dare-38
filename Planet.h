#ifndef PLANET_H
#define PLANET_H

#include <vector>

#include "GameObject.h" // Base class: GameObject
#include "Building.h"

class Building;

class Planet : public GameObject
{
	//sf::Texture* mtexture;		// Reference to texture; No need to copy
	
	float buildProgress;
	
	Building::StructureType beingBuilt;
	sf::Sprite buildingBeingConstructed;
	sf::Sprite progressBarCover;
	sf::Sprite progressBar;
	
	sf::CircleShape hitbox;
	
	int health;
	bool isPoisoned;
	
	bool hasScanner;
	bool hasShields;
	bool poweredDown;
	
	bool DEBUG_IS_INVULNERABLE;
	
	
	float baseEnergy;
	float baseRegenRate;
	float baseEnergyCap;
	
	
	float CannonBuildTime = 2.f;			// 15
	float ScannerBuildTime = 3.f;			// 30
	float MissileLauncherBuildTime = 5.f;	// 25
	
public:
	Planet();
	~Planet();
	
	float energyCap;
	float energyRegenRate;
	float currentEnergy;
	
	
	bool isBuilding;
	std::vector<Building> buildings;

	void Init();
	void Update(sf::Time _deltaTime);
	void Draw(sf::RenderWindow& _window);
	
	void ResetPlanet();
	void TakeDamage(int _dmg);
	
	void ConstructBuilding(Building::StructureType _construct);
	sf::CircleShape& GetHitbox();
	
	bool HasScanner();
	
	
	float GetEnergy();
	float GetEnergyCap();
	float GetEnergyRegenRate();
	
	int ArmedMissiles();
	
	bool isInFriendlyShieldBubble(sf::Vector2f _start);
	
	void ShieldHit(sf::Vector2f _start, float _damage);
};

#endif // PLANET_H
