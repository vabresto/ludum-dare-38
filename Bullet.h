#ifndef BULLET_H
#define BULLET_H

#include <SFML/Graphics.hpp>
#include "GameObject.h"

class Bullet : public GameObject
{
	sf::Vector2f direction;
	float moveSpeed;
	sf::Time timeToLive;

public:
	Bullet();
	~Bullet();

	void Init(sf::Vector2f _spawn, sf::Vector2f _dir, float _ms, sf::Time _life);
	void Update(sf::Time _deltaTime);
	bool IsStillAlive();
	
	void HitSomething();
};

#endif // BULLET_H
